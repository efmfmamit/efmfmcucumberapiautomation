package com.aventstack1.extentreports;

public interface IAnalysisStrategy {

    void setAnalysisStrategy(AnalysisStrategy strategy);

    AnalysisStrategy getAnalysisStrategy();

}
