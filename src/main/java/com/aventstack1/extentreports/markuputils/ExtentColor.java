package com.aventstack1.extentreports.markuputils;

public enum ExtentColor {
    RED,
    PINK,
    PURPLE,
    INDIGO,
    BLUE,
    CYAN,
    TEAL,
    GREEN,
    LIME,
    YELLOW,
    AMBER,
    ORANGE,
    BROWN,
    GREY,
    WHITE,
    BLACK,
    TRANSPARENT
}
