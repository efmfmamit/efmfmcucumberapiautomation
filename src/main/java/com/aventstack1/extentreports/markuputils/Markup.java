package com.aventstack1.extentreports.markuputils;

@FunctionalInterface
public interface Markup {
    String getMarkup();
}
