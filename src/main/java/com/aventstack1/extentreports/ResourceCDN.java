package com.aventstack1.extentreports;

/**
 * Allows selecting a CDN/resource loader for your HTML Reporter
 * <p>
 * Note: Some hosts do not allow loading resources via HTTPS protocol:
 * <p>
 * <ul>
 * <li>ExtentReports</li>
 * </ul>
 */
public enum ResourceCDN {
    GITHUB,
    EXTENTREPORTS
}
