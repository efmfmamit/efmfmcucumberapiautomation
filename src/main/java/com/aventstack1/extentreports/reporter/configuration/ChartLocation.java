package com.aventstack1.extentreports.reporter.configuration;

/**
 * For {@link com.aventstack1.extentreports.reporter.ExtentHtmlReporter}, sets the location of charts
 */
public enum ChartLocation {
    TOP,
    BOTTOM
}
