package com.aventstack1.extentreports.reporter.configuration;

/**
 * Available themes for the HTML reporter
 */
public enum Theme {
    STANDARD,
    DARK
}
