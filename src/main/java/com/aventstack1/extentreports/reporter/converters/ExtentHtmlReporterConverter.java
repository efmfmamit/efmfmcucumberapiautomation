package com.aventstack1.extentreports.reporter.converters;

import java.util.List;

import com.aventstack1.extentreports.model.Test;

public class ExtentHtmlReporterConverter {

    private String filePath;

    public ExtentHtmlReporterConverter(String filePath) {
        this.filePath = filePath;
    }

    public List<Test> parseAndGetModelCollection() {
        ExtentHtmlTestConverter converter = new ExtentHtmlTestConverter(filePath);
        List<Test> testList = converter.parseAndGetTests();
        return testList;
    }

}
