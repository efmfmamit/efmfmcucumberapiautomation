package com.aventstack1.extentreports.mediastorage;

import java.io.IOException;

import com.aventstack1.extentreports.model.Media;

public interface MediaStorage {
    void init(String v) throws IOException;

    void storeMedia(Media m) throws IOException;
}
