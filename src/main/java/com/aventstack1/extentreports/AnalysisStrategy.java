package com.aventstack1.extentreports;

public enum AnalysisStrategy {
    SUITE,
    CLASS,
    TEST
}
