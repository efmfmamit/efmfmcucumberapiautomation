package com.aventstack1.extentreports;

import com.aventstack1.extentreports.model.Media;

public class MediaEntityModelProvider {

    private Media m;

    public MediaEntityModelProvider(Media m) {
        this.m = m;
    }

    public Media getMedia() {
        return m;
    }

}
