package com.aventstack1.extentreports;

public interface RunResult {
    Status getStatus();
}
