package com.newtglobal.restapi.test.pojo;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.newtglobal.restapi.test.utils.RestUtils;
import org.apache.commons.lang3.builder.*;
import org.testng.Assert;

public class LaptopBag {

    private String brandName;
    private int Id;
    private String laptopName;
    private Features Features;


    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getLaptopName() {
        return laptopName;
    }

    public void setLaptopName(String laptopName) {
        this.laptopName = laptopName;
    }

    public Features getFeatures() {
        return Features;
    }

    public void setFeatures(Features features) {
        Features = features;
    }


    public String getDiffString(LaptopBag bag) {
        String diffStr = "";

        diffStr = (!(this.Id == bag.Id) ? "[ Id(Actual): '" + this.Id + "' Id_Expected '" + bag.Id + "' ]\n" : "") +
                (!(this.brandName.equals(bag.brandName)) ? "[ BrandName(Actual): '" + this.brandName + "'   ---   BrandName(Expected): '" + bag.brandName + "' ]\n" : "") +
                (!(this.laptopName.equals(bag.laptopName)) ? "[ LaptopName(Actual): '" + this.laptopName + "'   ---   LaptopName(Expected): '" + bag.laptopName + "' ]\n" : "") +
                (!(this.Features.getFeature().equals(bag.Features.getFeature())) ? "[ Features(Actual): '" + this.Features.getFeature() + "'   ---   Features(Expected): '" + bag.Features.getFeature() + "' ]\n" : "");

        return diffStr;
    }


    public static String ObjectToJsonStringMapper(LaptopBag bag) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setPropertyNamingStrategy(new PropertyNamingStrategy.UpperCamelCaseStrategy());
        try {
            return (mapper.writeValueAsString(bag));
        } catch (JsonProcessingException e) {
            Assert.fail("", e);
            return null;
        }
    }

    public static LaptopBag jsonStringToObjectMapper(String jsonString) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setPropertyNamingStrategy(new PropertyNamingStrategy.UpperCamelCaseStrategy());
        try {
            return mapper.readValue(jsonString, LaptopBag.class);
        } catch (java.io.IOException e) {
            Assert.fail("", e);
            return null;
        }
    }

}
