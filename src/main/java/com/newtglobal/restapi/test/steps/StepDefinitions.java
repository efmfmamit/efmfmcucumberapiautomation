package com.newtglobal.restapi.test.steps;

import com.newtglobal.restapi.test.conf.Config;
import com.newtglobal.restapi.test.utils.ExcelUtils;
import com.newtglobal.restapi.test.utils.RestUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.response.Response;

import java.util.LinkedHashMap;
import java.util.Map;

import org.testng.Assert;


public class StepDefinitions {

	Map<String, String> testData;

	@Given("^Service end point is active$")
	public void  applicationToBeLive() throws Throwable {

		System.out.println("Service end point is active");
		Response response = RestUtils.performGetRequest(Config.BASEURL + "/", "");
		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");		
		RestUtils.setSessionCookie(response);		
	}

	@Then("^Do Login$")
	public void  doLogin() throws Throwable {

		System.out.println("Do Login");
		Map<String, String> formData = new LinkedHashMap<>();
		formData.put("j_username", Config.getBaseUrl(Config.PROPERTYFILEPATH,"UserName"));
		formData.put("j_password", Config.getBaseUrl(Config.PROPERTYFILEPATH,"Password"));
		formData.put("_spring_security_remember_me", "true");
		formData.put("_spring_security_remember_me", "true");		
		Response response = RestUtils.performPostFormRequest(Config.BASEURL + "/j_spring_security_check", RestUtils.mapToJson(RestUtils.sessionCookiesAndAuthHeaders), formData);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "302");

		if(response.getStatusCode()==302)
		{	
			RestUtils.setSessionCookie(response);		
			String redirectUrl = response.getHeader("location");
			Response response1 = RestUtils.performGetRequest(Config.BASEURL+redirectUrl, RestUtils.mapToJson(RestUtils.sessionCookiesAndAuthHeaders));

			//Verify Response Code
			RestUtils.verifyStatuscode(response1, "200");

			//Extract  AuthToken And Session Cookie
			RestUtils.setSessionCookie(response1);
			RestUtils.setAuthTokenFromHTML(response1);
			RestUtils.testContextData.put("userId", RestUtils.setUserIdFromHTML(response1));
			RestUtils.testContextData.put("facility", RestUtils.setBranchIdFromHTML(response1));
			RestUtils.testContextData.put("CombinedFacility", RestUtils.setCombinedFacilityFromHTML(response1));
			RestUtils.testContextData.put("UserType","webuser");
			System.out.println("UserId :"+RestUtils.testContextData.get("userId"));
			System.out.println("Facility :"+RestUtils.testContextData.get("facility"));
			System.out.println("CombinedFacility :"+RestUtils.testContextData.get("CombinedFacility"));

		}

	}
	@Then("^Do Login Manager$")
	public void  doLoginManager() throws Throwable {

		System.out.println("Do Login");
		Map<String, String> formData = new LinkedHashMap<>();
		formData.put("j_username", Config.getBaseUrl(Config.PROPERTYFILEPATH,"MUserName"));
		formData.put("j_password", Config.getBaseUrl(Config.PROPERTYFILEPATH,"MPassword"));
		formData.put("_spring_security_remember_me", "true");
		formData.put("_spring_security_remember_me", "true");		
		Response response = RestUtils.performPostFormRequest(Config.BASEURL + "/j_spring_security_check", RestUtils.mapToJson(RestUtils.sessionCookiesAndAuthHeaders), formData);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "302");

		if(response.getStatusCode()==302)
		{	
			RestUtils.setSessionCookie(response);		
			String redirectUrl = response.getHeader("location");
			Response response1 = RestUtils.performGetRequest(Config.BASEURL+redirectUrl, RestUtils.mapToJson(RestUtils.sessionCookiesAndAuthHeaders));

			//Verify Response Code
			RestUtils.verifyStatuscode(response1, "200");

			//Extract  AuthToken And Session Cookie
			RestUtils.setSessionCookie(response1);
			RestUtils.setAuthTokenFromHTML(response1);
			RestUtils.testContextData.put("userId", RestUtils.setUserIdFromHTML(response1));
			RestUtils.testContextData.put("facility", RestUtils.setBranchIdFromHTML(response1));
			RestUtils.testContextData.put("CombinedFacility", RestUtils.setCombinedFacilityFromHTML(response1));
			RestUtils.testContextData.put("UserType", "Manager");
			System.out.println("UserId :"+RestUtils.testContextData.get("userId"));
			System.out.println("Facility :"+RestUtils.testContextData.get("facility"));
			System.out.println("CombinedFacility :"+RestUtils.testContextData.get("CombinedFacility"));

		}

	}
	@Then("^Do Login Spoc$")
	public void  doLoginSpoc() throws Throwable {

		System.out.println("Do Login");
		Map<String, String> formData = new LinkedHashMap<>();
		formData.put("j_username", Config.getBaseUrl(Config.PROPERTYFILEPATH,"SUserName"));
		formData.put("j_password", Config.getBaseUrl(Config.PROPERTYFILEPATH,"SPassword"));
		formData.put("_spring_security_remember_me", "true");
		formData.put("_spring_security_remember_me", "true");		
		Response response = RestUtils.performPostFormRequest(Config.BASEURL + "/j_spring_security_check", RestUtils.mapToJson(RestUtils.sessionCookiesAndAuthHeaders), formData);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "302");

		if(response.getStatusCode()==302)
		{	
			RestUtils.setSessionCookie(response);		
			String redirectUrl = response.getHeader("location");
			Response response1 = RestUtils.performGetRequest(Config.BASEURL+redirectUrl, RestUtils.mapToJson(RestUtils.sessionCookiesAndAuthHeaders));

			//Verify Response Code
			RestUtils.verifyStatuscode(response1, "200");

			//Extract  AuthToken And Session Cookie
			RestUtils.setSessionCookie(response1);
			RestUtils.setAuthTokenFromHTML(response1);
			RestUtils.testContextData.put("userId", RestUtils.setUserIdFromHTML(response1));
			RestUtils.testContextData.put("facility", RestUtils.setBranchIdFromHTML(response1));
			RestUtils.testContextData.put("CombinedFacility", RestUtils.setCombinedFacilityFromHTML(response1));
			RestUtils.testContextData.put("UserType", "SPOC");
			System.out.println("UserId :"+RestUtils.testContextData.get("userId"));
			System.out.println("Facility :"+RestUtils.testContextData.get("facility"));
			System.out.println("CombinedFacility :"+RestUtils.testContextData.get("CombinedFacility"));

		}

	}

	@Then("^Submit request settings_businessdays for_(.*)$")
	public void requestSettings(String dataID) throws Throwable  {
		String APIname="RequestSettings_businessdays";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);		
		System.out.println(testData.get("TestCaseTitle"));
		String requestpayLoad ="";

		requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");
		Response response =  RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),
				RestUtils.mapToJson(reHeaders),
				requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}

	@Then("^Submit individual settings for_(.*)$")
	public void individualSettings(String dataID) throws Throwable  {
		String APIname="IndividualSettings";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);		
		System.out.println(testData.get("TestCaseTitle"));
		String requestpayLoad ="";

		requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");
		Response response =  RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),
				RestUtils.mapToJson(reHeaders),
				requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}
	
	@Then("^Submit request settings_businessdays_negative for_(.*)$")
	public void requestSettingsNegative(String dataID) throws Throwable  {
		String APIname="RequestSettings_businessdays_Negative";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);		
		System.out.println(testData.get("TestCaseTitle"));
		String requestpayLoad ="";

		requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");
		Response response =  RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),
				RestUtils.mapToJson(reHeaders),
				requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}

	@Then("^Submit request settings_triptype for_(.*)$")
	public void requestSettingstriptype(String dataID) throws Throwable  {
		String APIname="RequestSettings_TripType";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);		
		System.out.println(testData.get("TestCaseTitle"));
		String requestpayLoad ="";

		requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");
		Response response =  RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),
				RestUtils.mapToJson(reHeaders),
				requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID,APIname,testData);	
	}

	@Then("^Submit request settings_triptype_negative for_(.*)$")
	public void requestSettingstriptypenegative(String dataID) throws Throwable  {
		String APIname="RequestSettings_TripType_Negative";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);		
		System.out.println(testData.get("TestCaseTitle"));
		String requestpayLoad ="";

		requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");
		Response response =  RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),
				RestUtils.mapToJson(reHeaders),
				requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID,APIname,testData);	
	}

	@Then("^Create a new request for cab booking for_(.*)$")
	public void BookACab(String dataID) throws Throwable {
		String APIname="BookCab_Positive";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		requestpayLoad=RestUtils.checkCurrentDateWeekend(requestpayLoad);
		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);
	}
	@Then("^Create a new request for cab booking for Weekend for_(.*)$")
	public void BookACabWeekend(String dataID) throws Throwable {
		String APIname="BookCab_PositiveWeekend";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);
	}

	@Then("^Create a new request for cab booking for Back2Back Request_(.*)$")
	public void BookACabB2B(String dataID) throws Throwable {
		String APIname="BookCab_Back2Back";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);
	}

	@Then("^Create a new request for cab booking negative scenario for_(.*)$")
	public void BookACabNegativeScenario(String dataID) throws Throwable {
		String APIname="BookCab_Negative";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);

		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("^View booking schedule for_(.*)$")
	public void ViewBookingSchedule(String dataID) throws Throwable 
	{
		String APIname="BookingSchedule";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers (session cookie and token) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);


		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.testContextData.put("Triptype", RestUtils.setTriptypeFromJson(response));
		//		System.out.println("Triptype :"+RestUtils.testContextData.get("Triptype"));
		//		RestUtils.testContextData.put("requestId", RestUtils.getrequestIdFromJson(response));
		//		System.out.println("requestId :"+RestUtils.testContextData.get("requestId"));
		RestUtils.storeJsonValueToTestContextDummy(response, testData.get("StoreJsonValue"),RestUtils.testContextData.get("Triptype"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}


	@Then("^Get Routing Details for_(.*)$")
	public void getRoutingDetails(String dataID) throws Throwable 
	{
		String APIname="GetRoutingDetails";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers (session cookie and token) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);


		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		//		RestUtils.testContextData.put("Triptype", RestUtils.setTriptypeFromJson(response));
		//		System.out.println("Triptype :"+RestUtils.testContextData.get("Triptype"));
		//		RestUtils.testContextData.put("requestId", RestUtils.getrequestIdFromJson(response));
		//		System.out.println("requestId :"+RestUtils.testContextData.get("requestId"));
		RestUtils.storeJsonValueToTestContextForFourvariables(response, testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Get Routing Details for Back2back Request_(.*)$")
	public void getRoutingDetailsB2B(String dataID) throws Throwable 
	{
		String APIname="GetRoutingDetailsB2B";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers (session cookie and token) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);


		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		//		RestUtils.testContextData.put("Triptype", RestUtils.setTriptypeFromJson(response));
		//		System.out.println("Triptype :"+RestUtils.testContextData.get("Triptype"));
		//		RestUtils.testContextData.put("requestId", RestUtils.getrequestIdFromJson(response));
		//		System.out.println("requestId :"+RestUtils.testContextData.get("requestId"));
		RestUtils.storeJsonValueToTestContextForFourvariables(response, testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Validate Cab Routing for_(.*)$")
	public void validateCabRouting(String dataID) throws Throwable 
	{
		String APIname="ValidateRouting";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers (session cookie and token) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);


		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

//		RestUtils.setAuthTokenFromJson(response);
		/*RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);*/
		RestUtils.storeJsonValueToTestContextForFourvariablesConcat(response, testData.get("StoreJsonValue"));

	}

	@Then("^Validate Cab Routing for back2back Routing_(.*)$")
	public void validateCabRoutingB2B(String dataID) throws Throwable 
	{
		String APIname="ValidateRoutingB2B";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers (session cookie and token) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);


		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		RestUtils.storeJsonValueToTestContextForFourvariablesConcat(response, testData.get("StoreJsonValue"));

	}

	@Then("^Allocate Vendor in a route for_(.*)$")
	public void allocateVendor(String dataID) throws Throwable 
	{
		String APIname="AllocateVendor";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers (session cookie and token) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Allocate new vendor in a route for_(.*)$")
	public void allocateNewVendor(String dataID) throws Throwable 
	{
		String APIname="AllocateNewVendor";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers (session cookie and token) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}
	
	@Then("^View DR booking schedule for_(.*)$")
	public void ViewDRBookingSchedule(String dataID) throws Throwable 
	{
		String APIname="DRBookingSchedule";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers (session cookie and token) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//System.out.println("RequestUrl: "+Config.BASEURL + testData.get("Request URL"));
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);


		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.testContextData.put("Triptype", RestUtils.setTriptypeFromJson(response));
		//System.out.println("Triptype :"+RestUtils.testContextData.get("Triptype"));
		//		RestUtils.testContextData.put("requestId", RestUtils.getrequestIdFromJson(response));
		//		System.out.println("requestId :"+RestUtils.testContextData.get("requestId"));
		RestUtils.storeJsonValueToTestContextDummy(response, testData.get("StoreJsonValue"),RestUtils.testContextData.get("Triptype"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^View SPOC booking schedule for_(.*)$")
	public void ViewSPOCBookingSchedule(String dataID) throws Throwable 
	{
		String APIname="SPOCBookingSchedule";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers (session cookie and token) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);


		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.testContextData.put("Triptype", RestUtils.setTriptypeFromJson(response));
		System.out.println("Triptype :"+RestUtils.testContextData.get("Triptype"));
		//		RestUtils.testContextData.put("requestId", RestUtils.getrequestIdFromJson(response));
		//		System.out.println("requestId :"+RestUtils.testContextData.get("requestId"));
		RestUtils.storeJsonValueToTestContextDummy(response, testData.get("StoreJsonValue"),RestUtils.testContextData.get("Triptype"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}	

	@Then("^Allocate cab in a route for_(.*)$")
	public void CabRouting(String dataID) throws Throwable {
		String APIname="CabRouting";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "204");

	}

	@Then("^Allocate cab in a route for back2back Request_(.*)$")
	public void CabRoutingPickup(String dataID) throws Throwable {
		String APIname="CabRoutingB2B";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "204");

	}

	@Then("^Allocate Adhoc cab in a route for_(.*)$")
	public void AdhocCabRouting(String dataID) throws Throwable {
		String APIname="AdhocCabRouting";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		//Verify Response Code
		RestUtils.verifyStatuscode(response, "204");

	}

	@Then("^Get Shift Id for_(.*)$")
	public void getShiftId(String dataID) throws Throwable{
		String APIname="GetShiftID";

		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.storeJsonValueToTestContextForShiftTimetwovariables(response, testData.get("StoreJsonValue"));

	}

	@Then("^Get Shift Id Weekend for_(.*)$")
	public void getShiftIdWeekend(String dataID) throws Throwable{
		String APIname="GetShiftIDForWeekend";

		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.storeJsonValueToTestContextForShiftTimetwovariables(response, testData.get("StoreJsonValue"));

	}
	@Then("^Get Shift Id for Back2Back Request_(.*)$")
	public void getShiftIdB2B(String dataID) throws Throwable{
		String APIname="GetShiftID_Back2Back";

		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.storeJsonValueToTestContextForShiftTimetwovariables(response, testData.get("StoreJsonValue"));

	}

	@Then("^Bulkrequest for cab Reshcedule for_(.*)$")
	public void BulkRequestForCabReshcedule(String dataID) throws  Throwable
	{	
		String APIname="BulkReschedule";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);


	}

	@Then("^Cancelcab for_(.*)$") 
	public void cancelcab(String dataID)throws Throwable
	{
		String APIname= "Cancelcab";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Rescheule cab for_(.*)$") 
	public void Reschedulecab(String dataID)throws Throwable
	{
		String APIname="Reschedulecab";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		requestpayLoad=RestUtils.checkCurrentDateWeekend(requestpayLoad);
		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Rescheule cab Weekend for_(.*)$") 
	public void ReschedulecabWeekend(String dataID)throws Throwable
	{
		String APIname="ReschedulecabWeekend";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}
	@Then("^Rescheule cab_negative for_(.*)$") 

	public void RescheuleCabnegative(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="Reschedulecab_Negative";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}
	@Then("^Cancelcabnegative for_(.*)$") 
	public void cancelcabnegative(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="Cancelcab_Negative";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);


	}
	@Then("^Able to view Adhoc request for_(.*)$")
	public void viewAdhocrequest(String dataID) throws Throwable {

		String APIname="ADHOC Request";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname );
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);


		//Verify Response Code
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);

		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}


	@Then("^Create adhoc request self for_(.*)$")

	public void adhocRequestSelf(String dataID)throws Throwable
	{
		String APIname="CreateADHOCRequestSelf";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname );
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Create adhoc request employee for_(.*)$")

	public void adhocRequestEmployee(String dataID)throws Throwable
	{
		String APIname="Create ADHOC Request Employee";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("^Reschedule adhoc request for_(.*)$")

	public void rescheduleadhocRequest(String dataID)throws Throwable
	{
		String APIname="Reschedule ADHOC Request";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		//RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);		
	}

	@Then("^Get Project Name Zone Name for_(.*)$")
	public void getProjectNameAndZoneName(String dataID)throws Throwable
	{
		String APIname="HelpDesk";
		//Read Excel data
		String newTemplateParameter="";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		if(RestUtils.testContextData.get("UserType").equalsIgnoreCase("SPOC"))
		{
			newTemplateParameter=testData.get("TemplateParameter").replace("$$UserName$$", "$$EmpUserNameSpocBooking$$");
		}
		else if (RestUtils.testContextData.get("UserType").equalsIgnoreCase("Manager"))
		{
			newTemplateParameter=testData.get("TemplateParameter").replace("$$UserName$$", "$$EmpUserNameManagerBooking$$");
		}
		else {
			newTemplateParameter=testData.get("TemplateParameter");
		}
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, newTemplateParameter);

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);

		RestUtils.testContextData.put("empProjectName", RestUtils.setProjectNameFromJson(response));
		RestUtils.testContextData.put("routeZoneName", RestUtils.setZoneNameFromJson(response));
		RestUtils.testContextData.put("employeeId", RestUtils.setEmployeeIdFromJson(response));
		//		System.out.println("Employee Project Name: "+RestUtils.testContextData.get("empProjectName"));
		//		System.out.println("Route Zone Name: "+RestUtils.testContextData.get("routeZoneName"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("^Get Process Id for_(.*)$")
	public void getProcessId(String dataID) throws Throwable{
		String APIname="GetProcessId";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get("ToGetProcessId");
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		//		String projectName = RestUtils.testContextData.get("empProjectName");
		String finalJsonPath = RestUtils.applyParametersToTemplateOrJsonPath(testData.get("StoreJsonValue"), testData.get("TemplateParameter"));
		RestUtils.storeJsonValueToTestContext(response, finalJsonPath);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Get Zone Id for_(.*)$")
	public void getZoneId(String dataID) throws Throwable{
		String APIname="GetZoneId";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get("ToGetZoneId");
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		String finalJsonPath = RestUtils.applyParametersToTemplateOrJsonPath(testData.get("StoreJsonValue"), testData.get("TemplateParameter"));
		RestUtils.storeJsonValueToTestContext(response, finalJsonPath);

		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Create shift Time for_(.*)$")
	public void createShiftTime(String dataID) throws  Throwable
	{
		String APIname="CreateShiftTime";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}
	
	@Then("^Create shift Time for Weekend for_(.*)$")
	public void createShiftTimeWeekend(String dataID) throws  Throwable
	{
		String APIname="CreateShiftTimeWeekend";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Validate shift Time for_(.*)$")
	public void validateShiftTime(String dataID) throws  Throwable
	{	
		String APIname="ValidateShiftTime";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContextForShiftTimetwovariables(response, testData.get("StoreJsonValue"));
		//RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	@Then("^Validate shift Time for Weekend for_(.*)$")
	public void validateShiftTimeWeekend(String dataID) throws  Throwable
	{	
		String APIname="ValidateShiftTimeWeekend";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContextForShiftTimetwovariables(response, testData.get("StoreJsonValue"));
		//RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	@Then("^Get Shift TimeemployeeRoster for_(.*)$")
	public void getShiftTimePickup(String dataID) throws  Throwable
	{	
		String APIname="GetShiftTime_EmployeeRoster";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");


		RestUtils.storeJsonValueToTestContextForShiftTime(response, testData.get("StoreJsonValue"));
		RestUtils.testContextData.put("PickupShiftTime", RestUtils.pickupshiftTime(response));
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}

	@Then("^Get Shift Time Routing Cab Allocation for_(.*)$")
	public void getShiftTimeCabAllocation(String dataID) throws  Throwable
	{	
		String APIname="GetShiftTime_RoutingCabAllocation";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");


		RestUtils.storeJsonValueToTestContextForShiftTimeConcat(response, testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}

	@Then("^Get Shift TimeDrop for_(.*)$")
	public void getShiftTimeDRop(String dataID) throws  Throwable
	{
		String APIname="GetShiftTime_DROP";			
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.storeJsonValueToTestContextForShiftTime(response, testData.get("StoreJsonValue"));
		RestUtils.testContextData.put("DropShiftTime", RestUtils.dropshiftTime(response));
		RestUtils.Validateresponse(response, dataID,APIname,testData);
	}

	@Then("^Able to view employee request for_(.*)$")
	public void employeeRequestSearch(String dataID) throws  Throwable
	{	
		String APIname="EmployeeRequestSearch";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");

		RestUtils.storeJsonValueToTestContextForTwovariables(response, testData.get("StoreJsonValue"));
		//		RestUtils.Validateresponse(response, dataID,APIname,testData);
	}

	@Then("^Able to search employee request based on employee ID for_(.*)$")
	public void employeeRequestSearchByEmployeeId(String dataID) throws  Throwable
	{	
		String APIname="EmployeeRequestSearchByEmployeeId";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.testContextData.put("Triptype", RestUtils.setTriptypeFromJson(response));
		System.out.println("Triptype :"+RestUtils.testContextData.get("Triptype"));
		//		RestUtils.testContextData.put("requestId", RestUtils.getrequestIdFromJson(response));
		//		System.out.println("requestId :"+RestUtils.testContextData.get("requestId"));
		RestUtils.storeJsonValueToTestContextDummy(response, testData.get("StoreJsonValue"),RestUtils.testContextData.get("Triptype"));		
	}

	@Then("^Able to downolad Employee Data for_(.*)$")
	public void DownloademployeeData(String dataID) throws  Throwable
	{
		String APIname="DownloadEmployeeData";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname );
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
	}

	@Then("^Able to download Employee Data based on Employee Id for_(.*)$")
	public void DownloademployeeDatabasedonEmployeeId(String dataID) throws  Throwable
	{
		String APIname="DownloadEmployeeDatabasedonEmployeeId";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname );
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
	}

	@Then("^Able to Bulkdelete employee data for_(.*)$")
	public void employeeRequestBulkDelete(String dataID) throws  Throwable
	{	
		String APIname="EmployeeRequestBulkDelete";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);
	}
	
	@Then("^Able to Bulkdelete based on RequestDetails for_(.*)$")
	public void employeeBulkDelete(String dataID) throws  Throwable
	{	
		String APIname="EmployeeRequestBulkDeletebasedonRequestDetails";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}
	
	@Then("^Able to delete all request for_(.*)$")
	public void bulkRequestDelete(String dataID) throws  Throwable
	{	
		String APIname="DeleteAllRequest";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}
	
	@Then("^Able to set Employee Details for_(.*)$")
	public void UpdateEmployeeSettings(String dataID) throws  Throwable
	{	
		String APIname="Update Employee Settings";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}
	@Then("^Get Shift Time Metro Exception for_(.*)$")
	public void getShiftTimeMetroException(String dataID) throws  Throwable
	{
		String APIname="GetShiftTime for Metro Exception";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);

		RestUtils.storeJsonValueToTestContextForShiftTime(response, testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID,APIname,testData);
	}
	@Then("^Able to Search Metro Exception for_(.*)$")
	public void MetroException(String dataID) throws  Throwable
	{
		String APIname="MetroException";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
	}
	@Then("^Able to Edit Travel Desk for_(.*)$")
	public void editTravelDesk(String dataID) throws  Throwable
	{	
		String APIname="Edit Travel Desk";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}
	@Then("^Able to UpdateEmployeeRoster for_(.*)$")
	public void  UpdateEmployeeRoster(String dataID) throws  Throwable
	{	
		String APIname="UpdateEmployeeRoster";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}
	@Then("^Able to delete EmployeeRoster single for_(.*)$")
	public void  DeleteEmployeeRoster(String dataID) throws  Throwable
	{	
		String APIname="EmployeeRosterDelete";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);
	}

	@Then("^Able to UpdateScheduleupdateDrop for_(.*)$")
	public void  UpdateScheduleupdateDrop(String dataID) throws  Throwable
	{	
		String APIname="UpdateScheduleupdateDROP";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}

	@Then("^Able to UpdateScheduleupdatePickup for_(.*)$")
	public void  UpdateScheduleupdatePickup(String dataID) throws  Throwable
	{	
		String APIname="UpdateScheduleupdatePickup";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}
	@Then("^Able to UpdateScheduleCancelPickup for_(.*)$")
	public void  UpdateScheduleCancelPickup(String dataID) throws  Throwable
	{	
		String APIname="UpdateScheduleCancelPICKUP";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}
	@Then("^Able to UpdateScheduleCancelDrop for_(.*)$")
	public void  UpdateScheduleCancelDrop(String dataID) throws  Throwable
	{	
		String APIname="UpdateScheduleCancelDROP";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);

	}
	@Then("^Able to EmployeeRequestAudit for_(.*)$")
	public void  EmployeeRequestAudit(String dataID) throws  Throwable
	{	
		String APIname="EmployeeRequestAudit";

		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
	}
	@Then("^Able to perform Employee Status Update for_(.*)$")
	public void  EmployeeStatusUpdate(String dataID) throws  Throwable
	{
		String APIname="EmployeeStatusUpdate";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.storeJsonValueToTestContext(response, testData.get("StoreJsonValue"));
	}

	@Then("^Able to Update Colleague Status for_(.*)$")
	public void  ColleagueReuestStatusUpdate(String dataID) throws  Throwable
	{	
		String APIname="Update Colleague Status";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID,APIname,testData);
	}
	@Then("^Able to set RememberRoute for_(.*)$")
	public void  rememberRouting(String dataID) throws  Throwable
	{	
		String APIname="RememberRoute";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}
	@Then("^Getting Vehicle details based on Facility for_(.*)$")
	public void vehicleDetailsbasedonFacility(String dataID) throws  Throwable
	{
		String APIname="Getting All Vehicles";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		RestUtils.storeJsonValueToTestContext(response, testData.get("StoreJsonValue"));

	}
	@Then("^Able to normalRoutenonnodal for_(.*)$")
	public void normalRoutenonnodal(String dataID) throws  Throwable
	{	
		String APIname="normalRoutenonnodal";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));

	}
	@Then("^Getting nodalroutes for_(.*)$")
	public void nodalRoute(String dataID) throws  Throwable
	{	
		String APIname="NodalRoute";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
//		RestUtils.setAuthTokenFromJson(response);
//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);		
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));


	}
	@Then("^Getting Request count for_(.*)$")
	public void gettingRequestCount(String dataID) throws  Throwable
	{	
		String APIname="GetNumberofRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, "GetNumberofRoutes");
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
//		RestUtils.setAuthTokenFromJson(response);
//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));


	}
	
	@Then("^createNormal Route for_(.*)$")
	public void createNormalRoute(String dataID) throws  Throwable
	{	
		String APIname="CreateRouteAdvancesearch";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
//		RestUtils.setAuthTokenFromJson(response);
//		RestUtils.setSessionCookie(response);
	}

	@Then("^Create Special Route for_(.*)$")
	public void createSpecialRoute(String dataID) throws  Throwable
	{	
		String APIname="CreateSpecialRoute";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
//		RestUtils.setAuthTokenFromJson(response);
//		RestUtils.setSessionCookie(response);
	}
	
	@Then("^Print all before complete routing for_(.*)$")
	public void printall_advancesearch(String dataID) throws  Throwable
	{	
		String APIname="PrintAll_Negative";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname );
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);

		//verify status code
		RestUtils.verifyStatuscode(response, "200");

		//validate response
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("^Print all after complete routing for_(.*)$")
	public void printAll(String dataID) throws  Throwable
	{	
		String APIname="PrintAll";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname );
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);

		//verify status code
		RestUtils.verifyStatuscode(response, "200");

	}

	@Then("^DummyCreate for_(.*)$")
	public void DummyCreate(String dataID) throws  Throwable
	{	
		String APIname="DummyCreate";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
	}

	@Then("^Able to perform DummyRoutePrintAll for_(.*)$")
	public void dummyRoutePrintAll(String dataID) throws  Throwable
	{	
		String APIname="DummyRoutePrintAll";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}
	@Then("^Able to create Back to Back for_(.*)$")
	public void createBackToBack(String dataID) throws  Throwable
	{	
		String APIname="CreateBacktoBack";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		//RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Able to perform Manual Back To Back Search for_(.*)$")
	public void manualBackToBack(String dataID) throws  Throwable
	{	
		String APIname="ManualBacktoBack_Search";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		RestUtils.storeJsonValueToTestContextForTwovariables(response, testData.get("StoreJsonValue"));

	}

	@Then("^Able to create Manual Back To Back Routing for_(.*)$")
	public void createManualBackToBack(String dataID) throws  Throwable
	{	
		String APIname="CreateManualBacktoBack";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//		RestUtils.setAuthTokenFromJson(response);
		//		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("^Able to perform Manual Back To Back Revoke for_(.*)$")
	public void manualBackToBackRevoke(String dataID) throws  Throwable
	{	
		String APIname="ManualBacktoBack_Revoke";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}
	@Then("^Able to perform Vendor distribution Search for_(.*)$")
	public void vendorDistributionSearch(String dataID) throws  Throwable
	{	
		String APIname="VendorDistribution_Search";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}
	@Then("^Able to get All_Routes for_(.*)$")
	public void All_Routes(String dataID) throws  Throwable
	{	
		String APIname="AllRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate count of employees yet to onboard for_(.*)$")
	public void yetToOnboard(String dataID) throws  Throwable
	{	
		String APIname="ValidateEmployeeYetToOnboard";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate total number of zone details_(.*)$")
	public void validateZoneSummary(String dataID) throws  Throwable
	{	
		String APIname="ValidateZoneSummary";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response, testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate total number of open route details_(.*)$")
	public void validateOpenRouteSummary(String dataID) throws  Throwable
	{	
		String APIname="ValidateOpenRouteSummary";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response, testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate total number of route details_(.*)$")
	public void validateRouteSummary(String dataID) throws  Throwable
	{	
		String APIname="ValidateRouteSummary";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response, testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate open vendor name for_(.*)$")
	public void validateOpenVendorName(String dataID) throws  Throwable
	{	
		String APIname="ValidateOpenVendorName";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate open vendor seating capacity for_(.*)$")
	public void validateOpenVendorSeatingCapacity(String dataID) throws  Throwable
	{	
		String APIname="ValidateOpenVendorSeatingCapacity";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}


	@Then("^Validate open vendor trip count for_(.*)$")
	public void validateOpenVendorCapacityCount(String dataID) throws  Throwable
	{	
		String APIname="ValidateOpenVendorTripCount";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}


	@Then("^Validate count of closed routes for_(.*)$")
	public void validateClosedRouteCount(String dataID) throws  Throwable
	{	
		String APIname="ValidateCountOfClosedRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}


	@Then("^Validate count of started routes for_(.*)$")
	public void validatestartedRouteCount(String dataID) throws  Throwable
	{	
		String APIname="ValidateCountOfStartedRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate count of closed but not started routes for_(.*)$")
	public void validateCountOfClosedButNotstartedRoute(String dataID) throws  Throwable
	{	
		String APIname="ValidateCountOfClosedButNotStartedRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate Capacity count of vehicle for_(.*)$")
	public void validateCapacityCountOfVehicle(String dataID) throws  Throwable
	{	
		String APIname="ValidateVehicleCapacityCount";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate Capacity of vehicle for_(.*)$")
	public void validateCapacityOfVehicle(String dataID) throws  Throwable
	{	
		String APIname="ValidateVehicleCapacity";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate count of Required Escorts for_(.*)$")
	public void validateCountOfRequiredEscorts(String dataID) throws  Throwable
	{	
		String APIname="ValidateCountOfRequiredEscorts";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}


	@Then("^Validate Vehicle Occupancy percentage for_(.*)$")
	public void vehicleOccupancyPercentage(String dataID) throws  Throwable
	{	
		String APIname="ValidateVehicleOccupancyPercentage";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate Total Employee Count for_(.*)$")
	public void totalEmployeeCount(String dataID) throws Throwable
	{	
		String APIname="ValidateTotalEmployeeCount";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate Total Male Employee Count for_(.*)$")
	public void totalMaleEmployeeCount(String dataID) throws  Throwable
	{	
		String APIname="ValidateTotalMaleEmployeeCount";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate Total female Employee Count for_(.*)$")
	public void totalfemaleEmployeeCount(String dataID) throws  Throwable
	{	
		String APIname="ValidateTotalFemaleEmployeeCount";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate Number of routes in a particular zone_(.*)$")
	public void numberOfRoutes(String dataID) throws  Throwable
	{	
		String APIname="ValidateNumberOfRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate vendor name for_(.*)$")
	public void validateVendorName(String dataID) throws  Throwable
	{	
		String APIname="ValidateVendorName";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate seating capacity for_(.*)$")
	public void validateSeatingCapacity(String dataID) throws  Throwable
	{	
		String APIname="ValidateSeatingCapacity";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate trip count for_(.*)$")
	public void validateTripCount(String dataID) throws  Throwable
	{	
		String APIname="ValidateTripCount";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate closed route after closing trip for_(.*)$")
	public void validateClosedRouteAfterClosingTrip(String dataID) throws  Throwable
	{	
		String APIname="ClosedRoute";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate closed route before closing trip for_(.*)$")
	public void validateClosedRouteBeforeClosingTrip(String dataID) throws  Throwable
	{	
		String APIname="ClosedRoute_Negative";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate closed route but not started for_(.*)$")
	public void validateClosedRouteButNotStarted(String dataID) throws  Throwable
	{	
		String APIname="ClosedButNotStarted";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}	

	@Then("^Validate started routes option for_(.*)$")
	public void validateStartedRoute(String dataID) throws  Throwable
	{	
		String APIname="StartedRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate open routes option for_(.*)$")
	public void validateOpenRoute(String dataID) throws  Throwable
	{	
		String APIname="OpenRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate google sequence option for_(.*)$")
	public void validateGoogleSequence(String dataID) throws  Throwable
	{	
		String APIname="GoogleSequence";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate back to back routes option for_(.*)$")
	public void validateback2backroutes(String dataID) throws  Throwable
	{	
		String APIname="BackToBackRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^Delete all routes for_(.*)$")
	public void deleteAllRoutes(String dataID) throws  Throwable
	{	
		String APIname="DeleteAllRoute";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}

	@Then("^View Routing Details for_(.*)$")
	public void viewRoutingDetails(String dataID) throws  Throwable
	{	
		String APIname="GetAllRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);	
	}

	@Then("^Validate added escort for_(.*)$")
	public void validateEscort(String dataID) throws  Throwable
	{	
		String APIname="ValidateEscort";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);	
	}
	
	@Then("^View Routing Details based on multiple facilities for_(.*)$")
	public void viewRoutingDetailsMultipleFacilities(String dataID) throws  Throwable
	{	
		String APIname="GetAllRoutesMultipleFacility";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);	
	}

	@Then("^Get route id for_(.*)$")
	public void getRouteId(String dataID) throws  Throwable
	{	
		String APIname="GetRouteId";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);	
	}

	@Then("^Get Escort checkIn Id for_(.*)$")
	public void getEscortCheckInId(String dataID) throws  Throwable
	{	
		String APIname="GetEscortCheckInId";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);	
	}

	@Then("^Add Escort for_(.*)$")
	public void addEscort(String dataID) throws  Throwable
	{	
		String APIname="AddEscort";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);	
	}


	@Then("^Get vehicles details for_(.*)$")
	public void getVehicleDetails(String dataID) throws  Throwable
	{	
		String APIname="GetVehicle";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContextFour(response, testData.get("StoreJsonValue"));
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);	
	}

	@Then("^Get vehicles and escorts details for_(.*)$")
	public void getEscortDetails(String dataID) throws  Throwable
	{	
		String APIname="GetEscort";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContextFive(response, testData.get("StoreJsonValue"));
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);	
	}
	
	@Then("Add vehicle to route for_(.*)$")
	public void addVehicle(String dataID) throws  Throwable
	{	
		String APIname="AddVehicle";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("Add vehicle and escort to route for_(.*)$")
	public void addVehicleAndEscort(String dataID) throws  Throwable
	{	
		String APIname="AddEscortToTrip";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	
	@Then("Add vehicle to new route for_(.*)$")
	public void addVehicleNewRoute(String dataID) throws  Throwable
	{	
		String APIname="AddVehicleNew";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("Add vehicle to route before billing cycle for_(.*)$")
	public void addVehicleBeforeBillingCycle(String dataID) throws  Throwable
	{	
		String APIname="AddVehicle_Negative";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("Add new vehicle for base facility_(.*)$")
	public void addNewVehicleBasefacility(String dataID) throws  Throwable
	{	
		String APIname="AddNewVehicleBase";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContextForTwovariables(response,testData.get("StoreJsonValue"));
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("Add new vehicle for other facility_(.*)$")
	public void addNewVehicleOtherFacility(String dataID) throws  Throwable
	{	
		String APIname="AddNewVehicleOther";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContextForTwovariables(response,testData.get("StoreJsonValue"));
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("Get unapproved vehicle list for base facility_(.*)$")
	public void getUnapprovedVehicle(String dataID) throws  Throwable
	{	
		String APIname="UnapprovedVehicleBase";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response, testData.get("StoreJsonValue"));
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
	}

	@Then("Get unapproved vehicle list for other facility_(.*)$")
	public void getUnapprovedVehicleOther(String dataID) throws  Throwable
	{	
		String APIname="UnapprovedVehicleOther";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response, testData.get("StoreJsonValue"));
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
	}

	@Then("Search unapproved vehicle for_(.*)$")
	public void searchVehiclenumber(String dataID) throws  Throwable
	{	
		String APIname="SearchUnapprovedVehicle";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Remove vehicle for_(.*)$")
	public void removeVehicle(String dataID) throws  Throwable
	{	
		String APIname="RemoveVehicle";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Close the route for_(.*)$")
	public void closeRoute(String dataID) throws  Throwable
	{	
		String APIname="CloseRoute";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Close the route with duplicate details for_(.*)$")
	public void closeRouteDuplicate(String dataID) throws  Throwable
	{	
		String APIname="CloseRoute_Duplicate";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Close the route before complete routing for_(.*)$")
	public void closeRouteBeforeRoutingComplete(String dataID) throws  Throwable
	{	
		String APIname="CloseRoute_Negative";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Close the route before driver allocation for_(.*)$")
	public void closeRouteBeforeDriverAllocation(String dataID) throws  Throwable
	{	
		String APIname="CloseRoute_DummyEntry";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Able to view closed routes for_(.*)$")
	public void viewClosedRoute(String dataID) throws  Throwable
	{	
		String APIname="BulkTripStart";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Able to view started trips for_(.*)$")
	public void viewStartedTrips(String dataID) throws  Throwable
	{	
		String APIname="ViewStartedTrips";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}
	
	@Then("Able to verify started routes for_(.*)$")
	public void viewStartedRoute(String dataID) throws  Throwable
	{	
		String APIname="VerifyStartedRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);	
	}
	
	@Then("Able to start all routes for_(.*)$")
	public void startAllRoutes(String dataID) throws  Throwable
	{	
		String APIname="StartAllTrips";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}
	
	@Then("Manually start the trip for_(.*)$")
	public void startTrip(String dataID) throws  Throwable
	{	
		String APIname="StartTrip";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("Manually start the trip with dummy entry for_(.*)$")
	public void startTripDummyEntry(String dataID) throws  Throwable
	{	
		String APIname="StartTripDummyEntry";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	
	@Then("Update employee status for_(.*)$")
	public void updateEmployeeStatus(String dataID) throws  Throwable
	{	
		String APIname="UpdateStatus";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Manually close the trip for_(.*)$")
	public void endTrip(String dataID) throws  Throwable
	{	
		String APIname="EndTrip";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Manually close the trip before status update for_(.*)$")
	public void endTripBeforeStatusUpdate(String dataID) throws  Throwable
	{	
		String APIname="EndTrip_BeforeStatusUpdate";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Search by route id for_(.*)$")
	public void normalSearchRouteId(String dataID) throws  Throwable
	{	
		String APIname="SearchRouteId";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Search by employee id for_(.*)$")
	public void normalSearchEmployeeId(String dataID) throws  Throwable
	{	
		String APIname="SearchEmployeeId";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Search by invalid employee id for_(.*)$")
	public void normalSearchInvalidEmployeeId(String dataID) throws  Throwable
	{	
		String APIname="SearchEmployeeId_Negative";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Search by employee id with valid route id for_(.*)$")
	public void normalSearchEmployeeIdRouteId(String dataID) throws  Throwable
	{	
		String APIname="SearchEmployeeIdByRouteId";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Search by vehicle number for_(.*)$")
	public void normalSearchVehicleId(String dataID) throws  Throwable
	{	
		String APIname="SearchVehicleNumber";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));

		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//	System.out.println(requestpayLoad);

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//Get vehicle number
		String vehicle = RestUtils.testContextData.get("vehicleNumber");
		String vehicleNo [] = vehicle.split("\\(");
		//	String stickerNo = vehicleNo[1].replace(")", "");
		//	System.out.println(stickerNo);
		//	String number = RestUtils.testContextData.put("vehicleNumber", stickerNo);
		//	System.out.println(number);
		requestpayLoad = requestpayLoad.replace(vehicle, vehicleNo[0]);
		//	System.out.println(requestpayLoad);

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Search by sticker number for_(.*)$")
	public void normalSearchStickerId(String dataID) throws  Throwable
	{	
		String APIname="SearchStickerNumber";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));

		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//	System.out.println(requestpayLoad);

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//Get vehicle number
		String vehicle = RestUtils.testContextData.get("vehicleNumber");
		String vehicleNo [] = vehicle.split("\\(");
		String stickerNo = vehicleNo[1].replace(")", "");
		//	System.out.println(stickerNo);
		//	String number = RestUtils.testContextData.put("vehicleNumber", stickerNo);
		//	System.out.println(number);
		requestpayLoad = requestpayLoad.replace(vehicle, stickerNo);
		//	System.out.println(requestpayLoad);

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Search by invalid route id for_(.*)$")
	public void normalSearchInvalidRouteID(String dataID) throws  Throwable
	{	
		String APIname="SearchRouteId_Negative";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Delete Route for_(.*)$")
	public void deleteRoute(String dataID) throws  Throwable
	{	
		String APIname="DeleteRoute";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Delete Route for Back2Back Request_(.*)$")
	public void deleteRouteB2B(String dataID) throws  Throwable
	{	
		String APIname="DeleteRouteB2B";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("Delete Route negative for_(.*)$")
	public void deleteRouteNegative(String dataID) throws  Throwable
	{	
		String APIname="DeleteRoute_Negative";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}

	@Then("^Able to perform TrackingCopy for_(.*)$")
	public void trackingCopy(String dataID) throws  Throwable
	{	
		String APIname="TrackingCopy";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);

	}
	
	@Then("^Able to download display sheet for_(.*)$")
	public void displaySheet(String dataID) throws  Throwable
	{	
		String APIname="DisplaySheet";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
	}

	@Then("^Able to download vendor wise tracking sheet for_(.*)$")
	public void vendorWiseTracking(String dataID) throws  Throwable
	{	
		String APIname="VendorWiseTracking";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
	}
	
	@Then("^Able to download routing sheet for_(.*)$")
	public void routingSheet(String dataID) throws  Throwable
	{	
		String APIname="RoutingSheet";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
	}
	
	@Then("^Able to download trip summary sheet for_(.*)$")
	public void tripSummarySheet(String dataID) throws  Throwable
	{	
		String APIname="TripSummarySheet";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
	}
	
	@Then("^Able to get routing dump on email for_(.*)$")
	public void routingDump(String dataID) throws  Throwable
	{	
		String APIname="RoutingDump";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
	}
	
	@Then("^Able to Complete Routing for_(.*)$")
	public void Complete_Routing(String dataID) throws  Throwable
	{	
		String APIname="RoutingComplete";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);

	}
	@Then("^Able to Prepare to Download for_(.*)$")
	public void Prepare_to_Download(String dataID) throws  Throwable
	{	
		String APIname="Prepare2Download";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);

	}
	@Then("Able to get vechile details swap employee for_(.*)$")
	public void vechicleDetailsSwapEmployee(String dataID) throws  Throwable
	{	
		String APIname="GetVechicleDetailsswapemployee";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));

	}
	@Then("Get project Id manager for_(.*)$")
	public void GetProjectId(String dataID) throws  Throwable
	{	
		String APIname="Get project Id manager";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		RestUtils.storeJsonValueToTestContextForShiftTimetwovariablesInt(response,testData.get("StoreJsonValue"));

	}
	@Then("Create a new request for cab booking manager for_(.*)$")
	public void cabBookingManager(String dataID) throws  Throwable
	{	
		String APIname="BookCabManager_Positive";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}
	@Then("Create a new request for cab booking manager negative for_(.*)$")
	public void cabBookingManagerNegative(String dataID) throws  Throwable
	{	
		String APIname="BookCabManager_Negative";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}
	@Then("Add Employee in a Route for_(.*)$")
	public void addEmployeeInRoute(String dataID) throws  Throwable
	{	
		String APIname="AddEmployeeRoute";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("Allocate taxi in a Route for_(.*)$")
	public void allocateTaxiRoute(String dataID) throws  Throwable
	{	
		String APIname="AllocateTaxiinaRoute";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("Able to get allActive vendors for_(.*)$")
	public void allActivevendors(String dataID) throws Throwable
	{	
		String APIname="swap_allactiveVendor";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		RestUtils.storeJsonValueToTestContextForShiftTime(response,testData.get("StoreJsonValue"));
	}
	
	@Then("Able to get delivered driver before close route for_(.*)$")
	public void deliveredDriverBeforeCloseRoute(String dataID) throws Throwable
	{	
		String APIname="DeliveredDevice_Negative";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
//		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	
	@Then("Able to get delivered driver after close route for_(.*)$")
	public void deliveredDriverAfterCloseRoute(String dataID) throws Throwable
	{	
		String APIname="DeliveredDevice";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
//		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	
	@Then("Able to get allocated but not closed routed details for_(.*)$")
	public void vehiclesAllocatedNotClosed(String dataID) throws Throwable
	{	
		String APIname="VehiclesAllocatedNotClosed";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
//		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	
	@Then("Able to get shuttle allocated trips for_(.*)$")
	public void shuttleTrips(String dataID) throws Throwable
	{	
		String APIname="ShuttleTrips";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
//		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	
	@Then("Able to get special routes for_(.*)$")
	public void specialRoutes(String dataID) throws Throwable
	{	
		String APIname="SpecialRoutes";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
//		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	
	@Then("Able to get escort required trips for_(.*)$")
	public void viewEscortRequiredTrips(String dataID) throws Throwable
	{	
		String APIname="EscortRequiredTrips";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
//		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	
	@Then("Able to view custom route for_(.*)$")
	public void customRoute(String dataID) throws Throwable
	{	
		String APIname="customRoute";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
	}

	@Then("Able to delete Swap route  for_(.*)$")
	public void deleteswaproute(String dataID) throws Throwable
	{	
		String APIname="Swap_Delete";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		//RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
	}

	@Then("Able to update Swap Route for_(.*)$")
	public void updateSwapRoute(String dataID) throws Throwable
	{	
		String APIname="UpdateSwapRoute";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("GetShiftTime_SPOC for_(.*)$")
	public void getShiftTimeSPOC(String dataID) throws Throwable
	{	
		String APIname="GetShiftTimeSPOC";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
		RestUtils.storeJsonValueToTestContextForShiftTimetwovariables(response, testData.get("StoreJsonValue"));
	}

	@Then("Get all emplyee Details_SPOC for_(.*)$")
	public void getallemployeeDetailsSPOC(String dataID) throws Throwable
	{	
		String APIname="GetallEmployeeDetailsSpoc";
		//Read Excel data
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	

		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	@Then("^Get Manager Booking Shift Id for_(.*)$")
	public void getBookingManagerShiftId(String dataID) throws Throwable{

		String APIname="GetShiftTimeforCabBookingManager";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID,APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.storeJsonValueToTestContextForShiftTimetwovariables(response, testData.get("StoreJsonValue"));

	}
	@Then("^Get Manager Reschdule Shift Id for_(.*)$")
	public void getReschduleManagerShiftId(String dataID) throws Throwable{
		String APIname="GetShiftTimeManager";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.storeJsonValueToTestContextForShiftTimetwovariables(response, testData.get("StoreJsonValue"));

	}

	@Then("^Reschedule cab Request Manger for_(.*)$") 
	public void RescheuleCabManager(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="ReschduleCabManger_Positive";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Reschdule cab Request Manger Negative for_(.*)$")
	public void RescheuleCabManagerNegative(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="ReschduleCabManger_Negative";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);

		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("^CancelcabnegativeManager for_(.*)$") 
	public void cancelcabnegativeManager(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="CancelCabManager_Negative";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("^CancelcabpositiveManager for_(.*)$") 
	public void cancelcabpositiveManager(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="CancelCabManager_Positive";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.setAuthTokenFromJson(response);
		RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("^Book a cab as spoc for_(.*)$") 
	public void BookCabSPOC(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="Book a cab SPOC";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("^Book a cab negative as spoc for_(.*)$") 
	public void BookCabSPOCNegative(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="Book a cab SPOC Negative";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}

	@Then("^Get SPOC Reschdule Shift Id for_(.*)$")
	public void getReschduleSPOCShiftId(String dataID) throws Throwable{
		String APIname="GetReschduleShiftTimeSPOC";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);
		RestUtils.storeJsonValueToTestContextForShiftTimetwovariables(response, testData.get("StoreJsonValue"));
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}
	@Then("^Reschedule cab  negative as SPOC for_(.*)$") 
	public void reschduleCabSPOCNegative(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="ReschedulecabSPOC_Negative";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);


	}
	@Then("Reschedule cab  positive as SPOC for_(.*)$") 
	public void reschduleCabSPOCPositive(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="Reschedulecab_SPOC";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);


	}
	@Then("CancelcabSPOC for_(.*)$") 
	public void cancelCabSPOCPositive(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="Cancelcab_SPOC";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);


	}
	@Then("CancelcabnegativeSPOC for_(.*)$") 
	public void cancelCabSPOCNegative(String dataID)throws Throwable
	{
		//Read Excel data
		String APIname="CancelcabSPOC_Negative";
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));
		//Read Template and Apply params
		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));

		//Clone existing headers ( session cookie and token ) and new  headers
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		//perform request
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"),  RestUtils.mapToJson(reHeaders), requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//RestUtils.setAuthTokenFromJson(response);
		//RestUtils.setSessionCookie(response);
		RestUtils.Validateresponse(response, dataID, APIname, testData);	
	}
	@Then("^Able to perform Geocode Routing for_(.*)$")
	public void getlistofProjects(String dataID) throws Throwable
	{
		String APIname="GeocodeRouting";

		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);

	}

	@Then("^Get freezecost id for_(.*)$")
	public void getFreezeCost(String dataID) throws Throwable
	{
		String APIname="GetFreezeCost";

		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response, testData.get("StoreJsonValue"));
		//	RestUtils.setAuthTokenFromJson(response);
		//	RestUtils.setSessionCookie(response);

	}

	@Then("^Close freezecost for_(.*)$")
	public void closeFreezeCost(String dataID) throws Throwable
	{
		String APIname="CloseFreezeCost";

		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Open freezecost for_(.*)$")
	public void openFreezeCost(String dataID) throws Throwable
	{
		String APIname="OpenFreezeCost";

		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);

	}

	@Then("^Create new route for_(.*)$")
	public void createNewRoute(String dataID) throws Throwable
	{
		String APIname="CreateNewRoute";

		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.storeJsonValueToTestContext(response,testData.get("StoreJsonValue"));
	}
	
	@Then("^Swap employee from one route to another route for_(.*)$")
	public void swapEmployee(String dataID) throws Throwable
	{
		String APIname="SwapEmployee";

		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	
	@Then("^Verify allocate vendor to route button for_(.*)$")
	public void allocatevendor(String dataID) throws Throwable
	{
		String APIname="AllocateVendorToRoute";

		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
	
	@Then("^Validate newly allocate vendor for_(.*)$")
	public void validateVendor(String dataID) throws Throwable
	{
		String APIname="ValidateVendor";

		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);
		System.out.println(testData.get("TestCaseTitle"));

		String requestpayLoad = Config.TEMPLATES.get(testData.get("TemplateName"));
		requestpayLoad = RestUtils.applyParametersToTemplateOrJsonPath(requestpayLoad, testData.get("TemplateParameter"));
		Map<String, String> reHeaders = new LinkedHashMap<>(RestUtils.sessionCookiesAndAuthHeaders);
		reHeaders.put("Content-Type", "application/json");	
		Response response = RestUtils.performPostRequest(Config.BASEURL + testData.get("Request URL"), RestUtils.mapToJson(reHeaders),requestpayLoad);
		RestUtils.verifyStatuscode(response, "200");
		RestUtils.Validateresponse(response, dataID, APIname, testData);
	}
}