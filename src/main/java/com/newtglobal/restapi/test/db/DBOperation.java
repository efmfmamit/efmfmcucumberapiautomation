package com.newtglobal.restapi.test.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.newtglobal.restapi.test.conf.Config;

public class DBOperation {
	public Integer UserId=null;
	public Integer BranchId=null;
	public Integer MANAGER_ID=null;
	public Integer ProjectId=null;
	
	public void createProjectInDB(String UserName) throws ClassNotFoundException, SQLException
	{
		
		// Get UserId and BranchId for the User
		ResultSet rset=ConnectToDB.execute("select UserId,BranchId,MANAGER_ID from eFmFmUserMaster where UserName='"+UserName+"';","S");
		while (rset.next())
		{
		 UserId=rset.getInt(1);
		
		 BranchId=rset.getInt(2);
		
		 MANAGER_ID= rset.getInt(3);
		 
		}
		rset.close();
	   //Check project exist for same branchId
		 
		 ResultSet getProjectEfmfm=ConnectToDB.execute("select ProjectId from eFmFmClientProjectDetails"
		 		+ " where BranchId="+BranchId+" and EmployeeProjectName='myproject'","S");
		 if(getProjectEfmfm.first() == true)
		 {
			 while (getProjectEfmfm.next())
				{
			 ProjectId=getProjectEfmfm.getInt(1);
				}
			 getProjectEfmfm.close();
			 ResultSet updateuserprojectemployeeproject=ConnectToDB.execute("update eFmFmEmployeeProjectDetailsPO set ProjectId='"+ProjectId+"' where UserId='"+UserId+"'","U");
			 updateuserprojectemployeeproject.close();
			 ResultSet updateuserprojectusermaster=ConnectToDB.execute("update eFmFmUserMaster set ProjectId='"+ProjectId+"' where UserId='"+UserId+"'","U");
			 updateuserprojectusermaster.close();
			 System.out.println("Project update successful with existing project");
		 }
		//Create one project 
		 else {
		ResultSet rset1=ConnectToDB.execute("Insert Into eFmFmClientProjectDetails (ClientProjectId, EmployeeProjectName, "
				+ "ProjectAllocationEndDate,ProjectAllocationStarDate,BranchId,"
				+ "CreatedDate ,IsActive,Remarks) Values('myproject','myproject','2017-06-06 16:00:00','2016-11-25 16:00:00','"+BranchId+"','2016-11-25 16:00:00','A',NULL )", "I");
		rset1.close();
		ResultSet reset2=ConnectToDB.execute("select Max(ProjectId) from eFmFmClientProjectDetails","S");
	    while (reset2.next())
	    {
	    ProjectId=reset2.getInt(1);
	    }
	    reset2.close();
		ResultSet rset3=ConnectToDB.execute("Select UserId from eFmFmEmployeeProjectDetailsPO where UserId='"+UserId+"'","S");
		System.out.println("New record inserted");
	    
		if(!(rset3==null))
	    {
	    	ResultSet rset4=ConnectToDB.execute("update eFmFmEmployeeProjectDetailsPO set ProjectId='"+ProjectId+"' where UserId='"+UserId+"'","U");
	    	ResultSet updateuserprojectusermaster=ConnectToDB.execute("update eFmFmUserMaster set ProjectId='"+ProjectId+"' where UserId='"+UserId+"'","U");
	    	rset3.close();
	    	rset4.close();
	    	updateuserprojectusermaster.close();
	    }
		else
		{
			ResultSet rset5=ConnectToDB.execute("INSERT INTO eFmFmEmployeeProjectDetailsPO (CreatedBy,CreatedDate,IsActive,"
					+ "LastModified,ModifiedDate,ProjectAllocationEndDate,ProjectAllocationStarDate,ReportingManagerUserId,ProjectId,"
					+ "UserId,DelegatedBy,DelegatedUserId,IsDelegatedUser,Remarks,BranchId,ModifiedEmpId)"
					+ "Values ('1','2018-07-14 02:09:1','Y', NULL, NULL,'2019-01-01 00:00:00','2018-01-01 00:00:00','"+MANAGER_ID+"','"+ProjectId+"','"+UserId+"',"
					+ "'0','0','0',NULL,'"+BranchId+"', NULL );","S");
			rset5.close();
		}
	}
		
	}
	 public static void main(String args[]) throws ClassNotFoundException, SQLException
	 {
		 DBOperation op= new DBOperation();
		 op.createProjectInDB(Config.getBaseUrl(Config.PROPERTYFILEPATH,"UserName"));
	 }
}
