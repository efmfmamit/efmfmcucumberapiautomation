package com.newtglobal.restapi.test.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.newtglobal.restapi.test.conf.Config;
public class ConnectToDB 
{
	
	static String filename="./Objects/EnvironmentDetails.properties";
	static String RDS;
	static String Schema;
	static String UserName;
	static String Password;
    public static ResultSet rs=null;
	static Connection conn;
	public static ResultSet execute(String query, String type) throws ClassNotFoundException, SQLException 
    {	
		
		RDS=Config.getBaseUrl(filename,"RDS");
		Schema=Config.getBaseUrl(filename,"Schema");
		UserName=Config.getBaseUrl(filename,"DBUserName");
		Password=Config.getBaseUrl(filename,"DBPassword");
    	
	conn=DriverManager.getConnection("jdbc:mysql://"+RDS+":3306"
			+ "/"+Schema+"", ""+UserName+"",""+Password+"");
	 Statement smt= conn.createStatement();
	try{if(type.equalsIgnoreCase("S"))
	    {rs=smt.executeQuery(query);
	    }
	   else if(type.equalsIgnoreCase("I"))
	   {smt.executeUpdate(query);	
		   }
	   else if(type.equalsIgnoreCase("D"))
	   {smt.executeUpdate(query);
	  }
	   else if(type.equalsIgnoreCase("U"))
   		    {smt.executeUpdate(query);
   		 }
	}
	catch( Exception e)
	{
		e.printStackTrace();
	}
	/*finally
	{
		conn.close();
	}*/
	return rs;     
	
    	}
}
