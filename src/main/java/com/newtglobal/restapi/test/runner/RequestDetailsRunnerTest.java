package com.newtglobal.restapi.test.runner;

import com.cucumber.listener.Reporter1;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberExceptionWrapper;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.CucumberFeatureWrapperImpl;
import cucumber.api.testng.TestNGCucumberRunner;
import cucumber.runtime.CucumberException;
import cucumber.runtime.model.CucumberFeature;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@CucumberOptions(
        features = {"./features/RequestDetails.feature"}
        , glue = "com.newtglobal.restapi.test.steps"
        , plugin = {"pretty:STDOUT",
        "html:Reports/cucumber-pretty",
        "json:Reports/cucumber-json/cucumber.json",
        "com.cucumber.listener.ExtentCucumberFormatter1:Reports/cucumber-extent/report.html"
}
        , monochrome = true
        , strict = true
        , dryRun = false
)
public class RequestDetailsRunnerTest {

    List<TestNGCucumberRunner> testNGCucumberRunnerList = new LinkedList<TestNGCucumberRunner>();

    @BeforeTest(alwaysRun = true)
    public void beforeTest() {

    }

    @BeforeClass
    public void setUp() throws Exception {

        /*WebDriver d = new ChromeDriver();
        d.findElement(By.xpath("")).sendKeys(Keys.TAB);
        d.switchTo().alert().sendKeys(Keys.TAB.toString());*/
    }

    @Test(dataProvider = "featureProvider")
    public void runner(CucumberFeatureWrapper cucumberFeatureWrapper) {
        TestNGCucumberRunner localRunner = new TestNGCucumberRunner(this.getClass());
        testNGCucumberRunnerList.add(localRunner);
        localRunner.runCucumber(cucumberFeatureWrapper.getCucumberFeature());
    }


    @AfterClass(alwaysRun = true)
    public void tearDownClass() throws Exception {

        Reporter1.loadXMLConfig(new File("extent-config.xml"));
        for (TestNGCucumberRunner testNGCucumberRunner : testNGCucumberRunnerList) {
            testNGCucumberRunner.finish();
        }

    }


    @DataProvider(name = "featureProvider")
    public Object[][] provideFeatures() {
        try {
            List<CucumberFeature> features = new TestNGCucumberRunner(this.getClass()).getFeatures();
            List<Object[]> featuresList = new ArrayList<Object[]>(features.size());
            for (CucumberFeature feature : features) {
                featuresList.add(new Object[]{new CucumberFeatureWrapperImpl(feature)});
            }
            return featuresList.toArray(new Object[][]{});
        } catch (CucumberException e) {
            return new Object[][]{new Object[]{new CucumberExceptionWrapper(e)}};
        }
    }
}