package com.newtglobal.restapi.test.utils;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.newtglobal.restapi.test.conf.Config;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 * @author Maqdoom Sharief
 * @date
 * @description This class is Util class for reading data from an excel source.
 */

public class ExcelUtils {

    private static final Logger LOGGER = Logger.getLogger(ExcelUtils.class.getName());

    private XSSFWorkbook excelWBook;
    private static ExcelUtils excelUtils = new ExcelUtils();
    private static int headerRow = 0;

    private ExcelUtils() {
        {
            try {
                // Open the Excel file
                FileInputStream excelFile = new FileInputStream(Config.EXCELPATH);
                // Access the required test data sheet
                excelWBook = new XSSFWorkbook(excelFile);
            } catch (Exception e) {
                LOGGER.error("Error while intializing the excel file " + e);
            }
        }
    }

    public static ExcelUtils getInstance() {
        return excelUtils;
    }


    private Sheet getSheet(String sheetName) {
        return excelWBook.getSheet(sheetName);
    }

    private List<String> getColumns(Sheet sheet) {
        final Row row = sheet.getRow(headerRow);
        final List<String> columnValues = new ArrayList<>();
        final int firstCellNum = row.getFirstCellNum();
        final int lastCellNum = row.getLastCellNum();
        for (int i = firstCellNum; i < lastCellNum; i++) {
            final Cell cell = row.getCell(i);
            if (cell != null)
                columnValues.add(cell.getStringCellValue());
            else
                columnValues.add("");
        }
        return columnValues;
    }

    public Map<String, String> getSheetData(String tcID, String sheetName) {
        System.out.println("TCID passed - " + tcID);
        System.out.println("SheetName passed - " + sheetName);
        final List<String> rowData = new ArrayList<>();
        final LinkedHashMap<String, String> rowVal = new LinkedHashMap<>();
        Object value;
        final Sheet sheet = getSheet(sheetName);
        final List<String> coulmnNames = getColumns(sheet);
        final int totalRows = sheet.getPhysicalNumberOfRows();
        final Row row = sheet.getRow(headerRow);
        final int firstCellNum = row.getFirstCellNum();
        final int lastCellNum = row.getLastCellNum();
        for (int i = headerRow + 1; i < totalRows; i++) {
            final Row rows = sheet.getRow(i);
            final String testLinkID = rows.getCell(0).getStringCellValue();
            if (tcID.equalsIgnoreCase(testLinkID)) {
                for (int j = firstCellNum; j < lastCellNum; j++) {
                    final Cell cell = rows.getCell(j);
                    if (cell == null
                            || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                        rowData.add("");
                    } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                        final Double val = cell.getNumericCellValue();
                        value = val.intValue();
                        rowData.add(value.toString());
                    } else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                        rowData.add(cell.getStringCellValue());
                    } else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
                        rowData.add(cell.getStringCellValue());
                    } else if (DateUtil.isCellDateFormatted(cell)) {
                        rowData.add(cell.getDateCellValue().toString());
                    } else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN
                            || cell.getCellType() == Cell.CELL_TYPE_ERROR
                            || cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
                        throw new RuntimeException(" Cell Type is not supported ");
                    }
                    rowVal.put(coulmnNames.get(j), rowData.get(j).trim().replace("{timestamp}", Config.TIMESTAMP + "" + Thread.currentThread().getId()));

                }
                break;
            }

        }
        return rowVal;

    }

    public List<Map<String, String>> getSheetAllData(String sheetName) {

        List<Map<String, String>> ret = new LinkedList<>();
        Object value;
        final Sheet sheet = getSheet(sheetName);
        final List<String> coulmnNames = getColumns(sheet);
        final int totalRows = sheet.getPhysicalNumberOfRows();
        final Row row = sheet.getRow(headerRow);
        final int firstCellNum = row.getFirstCellNum();
        final int lastCellNum = row.getLastCellNum();
        for (int i = headerRow + 1; i < totalRows; i++) {
            LinkedHashMap<String, String> rowVal = new LinkedHashMap<>();
            final Row rows = sheet.getRow(i);
            if(rows== null)
            	break;
            final List<String> rowData = new LinkedList<>();
            for (int j = firstCellNum; j < lastCellNum; j++) {
                final Cell cell = rows.getCell(j);
                if (cell == null
                        || cell.getCellType() == XSSFCell.CELL_TYPE_BLANK) {
                    rowData.add("");
                } else if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
                    final Double val = cell.getNumericCellValue();
                    value = val.intValue();
                    rowData.add(value.toString());
                } else if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
                    rowData.add(cell.getStringCellValue());
                } else if (cell.getCellType() == XSSFCell.CELL_TYPE_FORMULA) {
                    rowData.add(cell.getStringCellValue());
                } else if (DateUtil.isCellDateFormatted(cell)) {
                    rowData.add(cell.getDateCellValue().toString());
                } else if (cell.getCellType() == XSSFCell.CELL_TYPE_BOOLEAN
                        || cell.getCellType() == XSSFCell.CELL_TYPE_ERROR
                        || cell.getCellType() == XSSFCell.CELL_TYPE_FORMULA) {
                    throw new RuntimeException(" Cell Type is not supported ");
                }
                rowVal.put(coulmnNames.get(j), rowData.get(j).trim().replace("{timestamp}", Config.TIMESTAMP + "" + Thread.currentThread().getId()));
            }
            ret.add(rowVal);

        }
        return ret;

    }
    
    public Map<String, String> getSheetAllDataRelatedToDataIDAndModule(String sheetName, String dataID, String APIName) {
    
    	List<Map<String, String>> allrows = getSheetAllData(sheetName);
    	
    	for(Map<String, String> currRow : allrows)
    	{
    		if(currRow.get("Testcase_ID").equals(dataID) && currRow.get("APIName").equals(APIName))
    			return currRow;
    	}
		return null;    	
    }
} 
