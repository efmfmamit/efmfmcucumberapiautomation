package com.newtglobal.restapi.test.utils;

import com.cucumber.listener.Reporter1;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.newtglobal.restapi.test.conf.Config;
import com.newtglobal.restapi.test.pojo.LaptopBag;
import io.restassured.RestAssured;
import io.restassured.internal.support.Prettifier;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONObject;
import org.testng.Assert;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import groovy.util.Eval;

public class RestUtils {

	public static Map<String, String> sessionCookiesAndAuthHeaders = new LinkedHashMap<>();
	public static Map<String, String> testContextData = new LinkedHashMap<String, String>();
	Map<String, String> testData;


	public static Response performGetRequest(String url, String requestHeaders) {

		Response response = null;

		try {
			response = getCustomHeaders(requestHeaders).when().get(url);
			addReporterStepLog("Request Infomation",
					"<pre><b>Request URL:</b> <br/>" + url + "<br/><br/>"
							+ "<b>Method:</b> <br/>GET<br/><br/>"
							+ (!"".equals(requestHeaders) ? "<b>Request Headers:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(requestHeaders) : "") + "</pre>");
			addReporterStepLog("Response Infomation",
					"<pre><b>Status Code:</b> <br/>" + response.getStatusCode() + "<br/><br/>"
							+ (response.getHeaders().size() > 0 ? "<b>Response Headers:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(response.getHeaders().asList().toString()) + "<br/><br/>" : "<br/><br/>")
							+ (!"".equals(response.getBody().asString().trim()) ? "<b>Response Body:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(getPrettyPrintString(response)) : "") + "</pre>");

		} catch (Exception e) {
			Assert.fail("", e);
		}
		return response;
	}

	public static Response performPostRequest(String url, String requestHeaders, String requestBody) {

		Response response = null;

		try {

			response = getCustomHeaders(requestHeaders).body(requestBody).post(url);

			addReporterStepLog("Request Infomation",
					"<pre><b>Request URL:</b> <br/>" + url + "<br/><br/>"
							+ "<b>Method:</b> <br/>POST<br/><br/>"
							+ (!"".equals(requestBody) ? "<b>Request Body:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(requestBody) + "<br/><br/>" : "")
							+ (!"".equals(requestHeaders) ? "<b>Request Headers:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(requestHeaders) + "</pre>" : ""));
			addReporterStepLog("Response Infomation",
					"<pre><b>Status Code:</b> <br/>" + response.getStatusCode() + "<br/><br/>"
							+ (response.getHeaders().size() > 0 ? "<b>Response Headers:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(response.getHeaders().asList().toString()) + "<br/><br/>" : "<br/><br/>")
							+ (!"".equals(response.getBody().asString().trim()) ? "<b>Response Body:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(getPrettyPrintString(response)) : "") + "</pre>");
		} catch (Exception e) {
			Assert.fail("", e);
		}
		return response;

	}




	public static Response performPostFormRequest(String url, String requestHeaders, Map<String, String> formdata) {

		Response response = null;

		try {

			response = getCustomHeaders(requestHeaders).formParams(formdata).post(url);

			addReporterStepLog("Request Infomation",
					"<pre><b>Request URL:</b> <br/>" + url + "<br/><br/>"
							+ "<b>Method:</b> <br/>POST<br/><br/>"
							+ (!"".equals(getDataFormUrlencodedString(formdata)) ? "<b>Request Body:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(getDataFormUrlencodedString(formdata)) + "<br/><br/>" : "")
							+ (!"".equals(requestHeaders) ? "<b>Request Headers:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(requestHeaders) + "</pre>" : ""));
			addReporterStepLog("Response Infomation",
					"<pre><b>Status Code:</b> <br/>" + response.getStatusCode() + "<br/><br/>"
							+ (response.getHeaders().size() > 0 ? "<b>Response Headers:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(response.getHeaders().asList().toString()) + "<br/><br/>" : "<br/><br/>")
							+ (!"".equals(response.getBody().asString().trim()) ? "<b>Response Body:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(getPrettyPrintString(response)) : "") + "</pre>");
		} catch (Exception e) {
			Assert.fail("", e);
		}
		return response;

	}

	public static Response performPutRequest(String url, String requestHeaders, String requestBody) {

		Response response = null;

		try {

			response = getCustomHeaders(requestHeaders).body(requestBody).put(url);

			addReporterStepLog("Request Infomation",
					"<pre><b>Request URL:</b> <br/>" + url + "<br/><br/>"
							+ "<b>Method:</b> <br/>PUT<br/><br/>"
							+ (!"".equals(requestBody) ? "<b>Request Body:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(requestBody) + "<br/><br/>" : "")
							+ (!"".equals(requestHeaders) ? "<b>Request Headers:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(requestHeaders) + "</pre>" : ""));
			addReporterStepLog("Response Infomation",
					"<pre><b>Status Code:</b> <br/>" + response.getStatusCode() + "<br/><br/>"
							+ (response.getHeaders().size() > 0 ? "<b>Response Headers:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(response.getHeaders().asList().toString()) + "<br/><br/>" : "<br/><br/>")
							+ (!"".equals(response.getBody().asString().trim()) ? "<b>Response Body:</b> <br/>"
									+ StringEscapeUtils.escapeHtml3(getPrettyPrintString(response)) : "") + "</pre>");
		} catch (Exception e) {
			Assert.fail("", e);
		}
		return response;

	}


	public static RequestSpecification getCustomHeaders(String requestHeaders) {

		RequestSpecification request = null;

		try {


			request = RestAssured.given().log().ifValidationFails();
			if (requestHeaders != null && !requestHeaders.isEmpty()) {

				ObjectMapper mapper = new ObjectMapper();
				Map<String, Object> map = new HashMap<String, Object>();
				map = mapper.readValue(requestHeaders, new TypeReference<Map<String, String>>() {
				});

				request.headers(map);

			}

		} catch (Exception e) {
			Assert.fail("", e);
		}

		return request;
	}

	public static void verifyStatuscode(Response responseObj, String responseCode) {
		try {


			if (responseObj.getStatusCode() == Integer.parseInt(responseCode))
				addReporterStepLog("Statuscode - Value Should be matched",
						"<b>Actual Statuscode:  </b>" + responseObj.getStatusCode() + " is as same as Expected");
			else {
				addReporterStepLogFailed("Statuscode - Value Should be matched",
						"Actual Statuscode is as NOT same as Expected" + "<br/><b>ExpectedVaule: </b>" + responseCode
						+ "<br/><b>ActualVaule: </b>" + responseObj.getStatusCode());
				Assert.fail("Actual Statuscode is as NOT same as Expected");
			}
		} catch (Exception e) {

			Assert.fail("", e);
		}

	}

	public static void verifyHeaders(Response responseObj, String headersKVExpectedString) {
		try {
			String errorString = "";

			Map<String, String> headersKVExpected = new ObjectMapper().readValue(headersKVExpectedString, new TypeReference<Map<String, String>>() {
			});
			for (String header : headersKVExpected.keySet()) {
				if (responseObj.getHeaders().hasHeaderWithName(header)) {

					if (responseObj.getHeaders().getValue(header).equals(headersKVExpected.get(header)))
						addReporterStepLog("Header Key-Value Should be matched in the response",
								"Header '" + header + "' with vaule  '" + headersKVExpected.get(header) + "' matched in the response");

					else
						errorString = errorString + "Header '" + header + "' with vaule not matched in the response. ExpectedVaule: '" + headersKVExpected.get(header)
						+ "'. ActualValue: '" + responseObj.getHeaders().getValue(header) + "' <br/>";

				} else
					errorString = errorString + "Header '" + header + "' is missing in the response<br/>";
			}
			if (!errorString.isEmpty())
				Assert.fail(errorString);

		} catch (Exception e) {

			Assert.fail("", e);
		}

	}

	static public void addReporterStepLog(String stepTitle, String logMsg) {
		Reporter1.addStepLog("<h6 onclick=\"$(this).nextAll().slideToggle(50, function () {});\" style=\"cursor: pointer;\">" + stepTitle + ":</h6>" +
				"<br/><font color=\"blue\">" + logMsg + "</font>");
	}

	static public void addReporterStepLogFailed(String stepTitle, String logMsg) {
		Reporter1.addStepLog("<h6 onclick=\"$(this).nextAll().slideToggle(50, function () {});\" style=\"cursor: pointer;\">" + stepTitle + ":</h6>" +
				"<br/><font color=\"red\">" + logMsg + "</font>");
	}


	static private String getPrettyPrintString(Response response) {
		String s = new Prettifier().getPrettifiedBodyIfPossible((ResponseOptions) response.body(), response.body());
		return s;
	}


	static private String getDataFormUrlencodedString(Map<String, String> params) throws Exception{
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for(Map.Entry<String, String> entry : params.entrySet()){
			if (first)
				first = false;
			else
				result.append("&");    
			result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
		}    
		return result.toString();
	}

	public static void setAuthTokenFromHTML(Response response)
	{
		String token = null;
		Pattern pat = Pattern.compile("var authenticationToken =\\\"(.*)\\\";");
		Matcher matcher = pat.matcher(response.getBody().asString());
		if(matcher.find())
			token = matcher.group(1);
		else
			token = "";

		if(token!=null)
			sessionCookiesAndAuthHeaders.put("authenticationToken", token);
		//		System.out.println(token);
	}

	public static String setCombinedFacilityFromHTML(Response response)
	{
		//		String digits;
		String combinedFacility = null;
		Pattern pat = Pattern.compile("var combinedFacility = \\\"(.*)\\\";");		
		Matcher matcher = pat.matcher(response.getBody().asString());
		if(matcher.find())
		{
			combinedFacility = matcher.group(1);
			//remove special character
			//digits = userId.replaceAll("[^0-9.]", "");
			return combinedFacility;
		}
		else
		{
			return combinedFacility = "";
		}

	}

	public static String setUserIdFromHTML(Response response)
	{
		String digits;
		String userId = null;
		Pattern pat = Pattern.compile("var profileId =(.*);");		
		Matcher matcher = pat.matcher(response.getBody().asString());
		if(matcher.find())
		{
			userId = matcher.group(1);
			digits = userId.replaceAll("[^0-9.]", "");
			return digits;
		}
		else
		{
			return digits = "";
		}
		/*if(token!=null)
   			sessionCookiesAndAuthHeaders.put("authenticationToken", token);	*/	
	}

	public static String setBranchIdFromHTML(Response response)
	{
		String digits;
		String userId = null;
		Pattern pat = Pattern.compile("var branchId =(.*);");		
		Matcher matcher = pat.matcher(response.getBody().asString());
		if(matcher.find())
		{
			userId = matcher.group(1);
			digits = userId.replaceAll("[^0-9.]", "");
			return digits;
		}
		else
		{
			return digits = "";
		}
		/*if(token!=null)
   			sessionCookiesAndAuthHeaders.put("authenticationToken", token);	*/	
	}

	public static String setProjectNameFromJson(Response response)
	{
		String projectName = response.jsonPath().getString("empProfileDetail.empProjectName");
		return projectName;
	}

	public static String setZoneNameFromJson(Response response)
	{
		String zoneName = response.jsonPath().getString("empProfileDetail.routeZoneName");
		return zoneName;
	}

	public static String setEmployeeIdFromJson(Response response)
	{
		String employeeId = response.jsonPath().getString("empProfileDetail.employeeId");
		return employeeId;
	}

	//	public static String getrequestIdFromJson(Response response)
	//	{
	//		String requestId = response.jsonPath().getString("requests[0].requestId");
	//		return requestId;
	//	}

	public static String dropshiftTime(Response response)
	{
		String shiftTime = response.jsonPath().getString("shift.findAll{it.shiftTime=='18:00'}[0].shiftTime");
		String dropshiftTime=shiftTime.concat(":00");
		return dropshiftTime;
	}
	public static void setAuthTokenFromJson(Response response)
	{
		String token = response.jsonPath().getString("token");
		//		System.out.println(token);
		if(token!=null)
			sessionCookiesAndAuthHeaders.put("authenticationToken", token);		
	}

	public static void setSessionCookie(Response response)
	{
		if(response.getCookie("JSESSIONID")!=null)
			sessionCookiesAndAuthHeaders.put("Cookie", "JSESSIONID="+response.getCookie("JSESSIONID"));
	}

	public static String mapToJson(Map<String, String> map)
	{
		return new JSONObject(map).toString();
	}

	public static Map<String, String> jsonToMap(String jsonStr) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> map = new HashMap<String, String>();
		map = mapper.readValue(jsonStr, new TypeReference<Map<String, String>>() {
		});
		return map;
	}

	public static void storeJsonValueToTestContext(Response response, String storeJsonValueString) throws Exception
	{
		Map<String, String> map = RestUtils.jsonToMap(storeJsonValueString);

		if(map.containsKey("JsonPath") && map.containsKey("TestContextKeyName"))
		{
			//System.out.println(response.body().asString());
			Object obj = response.jsonPath().get(map.get("JsonPath"));
			String jsonPathValue = "";
			if(obj instanceof Integer)
			{
				jsonPathValue = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath")));

			}
			else
				jsonPathValue = String.valueOf((String)response.jsonPath().get(map.get("JsonPath")));

			testContextData.put(map.get("TestContextKeyName"), jsonPathValue);
		}
	}

	public static String pickupshiftTime(Response response)
	{
		String shiftTime = response.jsonPath().getString("shift.findAll{it.shiftTime=='18:00'}[0].shiftTime");
		String pickupshiftTime=shiftTime.concat(":00");
		return pickupshiftTime;
	}

	public static void storeJsonValueToTestContextForShiftTimeConcat(Response response, String storeJsonValueString) throws Exception
	{
		Map<String, String> map = RestUtils.jsonToMap(storeJsonValueString);

		if(map.containsKey("JsonPath") && map.containsKey("TestContextKeyName")||(map.containsKey("JsonPath1") && map.containsKey("TestContextKeyName1")))
		{
			Object obj = response.jsonPath().get(map.get("JsonPath"));

			String time = obj.toString();

			String concat = time.concat(":00");
			//			System.out.println("Shifttime"+obj);
			testContextData.put(map.get("TestContextKeyName"), (String) concat);
		}
	}

	public static String setTriptypeFromJson(Response response)
	{
		String tripType="";
		//ResponseBody body = response.getBody();
		//System.out.println("Response Body is: " + body.asString());
		tripType = response.jsonPath().getString("requests[0].tripType");
		return tripType;
	}
	
	public static void storeJsonValueToTestContextDummy(Response response, String storeJsonValueString, String dynamicValue) throws Exception
	{
		String finalstr = null;

		Map<String, String> mapdata = RestUtils.jsonToMap(storeJsonValueString);

		if(mapdata.containsKey("JsonPath") && mapdata.containsKey("TestContextKeyName"))
		{
			for(int i=0;i<=1;i++)
			{
				finalstr= (mapdata.get("JsonPath")).replace("<<dynamicpattern>>",dynamicValue);
			}
			//System.out.println(response.body().asString());
			Object obj = response.jsonPath().get(finalstr);
			String jsonPathValue = "";


			if(obj instanceof Integer)
			{
				jsonPathValue = String.valueOf((Integer)response.jsonPath().get(finalstr));

			}
			else
				jsonPathValue = String.valueOf(response.jsonPath().get(finalstr));

			testContextData.put(mapdata.get("TestContextKeyName"), jsonPathValue);
		}
	}

	public static void storeJsonValueToTestContextForTwovariables(Response response, String storeJsonValueString) throws Exception
	{
		Map<String, String> map = RestUtils.jsonToMap(storeJsonValueString);

		if(map.containsKey("JsonPath") && map.containsKey("TestContextKeyName") && (map.containsKey("JsonPath1") && map.containsKey("TestContextKeyName1")))
		{
			//System.out.println(response.body().asString());
			Object obj = response.jsonPath().get(map.get("JsonPath"));
			Object obj1 = response.jsonPath().get(map.get("JsonPath1"));
			String jsonPathValue = "";
			String jsonValue1 = "";
			if(obj instanceof Integer)
			{
				jsonPathValue = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath")));

			}
			else
				jsonPathValue = String.valueOf(response.jsonPath().get(map.get("JsonPath")));

			testContextData.put(map.get("TestContextKeyName"), jsonPathValue);

			if(obj1 instanceof Integer)
			{
				jsonValue1 = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath1")));

			}
			else
				jsonValue1 = String.valueOf((String)response.jsonPath().get(map.get("JsonPath1")));

			testContextData.put(map.get("TestContextKeyName1"), jsonValue1);
		}
	}

	public static void storeJsonValueToTestContextForThreevariables(Response response, String storeJsonValueString) throws Exception
	{
		Map<String, String> map = RestUtils.jsonToMap(storeJsonValueString);

		if(map.containsKey("JsonPath") && map.containsKey("TestContextKeyName") && (map.containsKey("JsonPath1") && map.containsKey("TestContextKeyName1")) && (map.containsKey("JsonPath2") && map.containsKey("TestContextKeyName2")))
		{
			//System.out.println(response.body().asString());
			Object obj = response.jsonPath().get(map.get("JsonPath"));
			Object obj1 = response.jsonPath().get(map.get("JsonPath1"));
			Object obj2 = response.jsonPath().get(map.get("JsonPath2"));
			String jsonPathValue = "";
			String jsonValue1 = "";
			String jsonValue2 = "";
			if(obj instanceof Integer)
			{
				jsonPathValue = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath")));

			}
			else
				jsonPathValue = String.valueOf(response.jsonPath().get(map.get("JsonPath")));

			testContextData.put(map.get("TestContextKeyName"), jsonPathValue);

			if(obj1 instanceof Integer)
			{
				jsonValue1 = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath1")));

			}
			else
				jsonValue1 = String.valueOf((String)response.jsonPath().get(map.get("JsonPath1")));

			testContextData.put(map.get("TestContextKeyName1"), jsonValue1);

			if(obj2 instanceof Integer)
			{
				jsonValue2 = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath2")));

			}
			else
				jsonValue2 = String.valueOf((String)response.jsonPath().get(map.get("JsonPath2")));

			testContextData.put(map.get("TestContextKeyName2"), jsonValue2);
		}
	}

	public static void storeJsonValueToTestContextFour(Response response, String storeJsonValueString) throws Exception
	{
		Map<String, String> map = RestUtils.jsonToMap(storeJsonValueString);

		if(map.containsKey("JsonPath") && map.containsKey("TestContextKeyName") && (map.containsKey("JsonPath1") && map.containsKey("TestContextKeyName1")) 
				&& (map.containsKey("JsonPath2") && map.containsKey("TestContextKeyName2")) && 
				(map.containsKey("JsonPath3") && map.containsKey("TestContextKeyName3")))
		{
			//System.out.println(response.body().asString());
			Object obj = response.jsonPath().get(map.get("JsonPath"));
			Object obj1 = response.jsonPath().get(map.get("JsonPath1"));
			Object obj2 = response.jsonPath().get(map.get("JsonPath2"));
			Object obj3 = response.jsonPath().get(map.get("JsonPath3"));
			String jsonPathValue = "";
			String jsonValue1 = "";
			String jsonValue2 = "";
			String jsonValue3 = "";
			if(obj instanceof Integer)
			{
				jsonPathValue = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath")));

			}
			else
				jsonPathValue = String.valueOf(response.jsonPath().get(map.get("JsonPath")));

			testContextData.put(map.get("TestContextKeyName"), jsonPathValue);

			if(obj1 instanceof Integer)
			{
				jsonValue1 = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath1")));

			}
			else
				jsonValue1 = String.valueOf(response.jsonPath().get(map.get("JsonPath1")));

			testContextData.put(map.get("TestContextKeyName1"), jsonValue1);

			if(obj2 instanceof Integer)
			{
				jsonValue2 = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath2")));

			}
			else
				jsonValue2 = String.valueOf(response.jsonPath().get(map.get("JsonPath2")));

			testContextData.put(map.get("TestContextKeyName2"), jsonValue2);

			if(obj3 instanceof Integer)
			{
				jsonValue3 = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath3")));

			}
			else
				jsonValue3 = String.valueOf((String)response.jsonPath().get(map.get("JsonPath3")));

			testContextData.put(map.get("TestContextKeyName3"), jsonValue3);
		}
	}

	public static void storeJsonValueToTestContextFive(Response response, String storeJsonValueString) throws Exception
	{
		Map<String, String> map = RestUtils.jsonToMap(storeJsonValueString);

		if(map.containsKey("JsonPath") && map.containsKey("TestContextKeyName") && (map.containsKey("JsonPath1") && map.containsKey("TestContextKeyName1")) &&
				(map.containsKey("JsonPath2") && map.containsKey("TestContextKeyName2")) && (map.containsKey("JsonPath3") && map.containsKey("TestContextKeyName3"))
				&& (map.containsKey("JsonPath4") && map.containsKey("TestContextKeyName4")))
		{
			//System.out.println(response.body().asString());
			Object obj = response.jsonPath().get(map.get("JsonPath"));
			Object obj1 = response.jsonPath().get(map.get("JsonPath1"));
			Object obj2 = response.jsonPath().get(map.get("JsonPath2"));
			Object obj3 = response.jsonPath().get(map.get("JsonPath3"));
			Object obj4 = response.jsonPath().get(map.get("JsonPath4"));
			String jsonPathValue = "";
			String jsonValue1 = "";
			String jsonValue2 = "";
			String jsonValue3 = "";
			String jsonValue4 = "";
			if(obj instanceof Integer)
			{
				jsonPathValue = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath")));

			}
			else
				jsonPathValue = String.valueOf(response.jsonPath().get(map.get("JsonPath")));

			testContextData.put(map.get("TestContextKeyName"), jsonPathValue);

			if(obj1 instanceof Integer)
			{
				jsonValue1 = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath1")));

			}
			else
				jsonValue1 = String.valueOf(response.jsonPath().get(map.get("JsonPath1")));

			testContextData.put(map.get("TestContextKeyName1"), jsonValue1);

			if(obj2 instanceof Integer)
			{
				jsonValue2 = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath2")));

			}
			else
				jsonValue2 = String.valueOf(response.jsonPath().get(map.get("JsonPath2")));

			testContextData.put(map.get("TestContextKeyName2"), jsonValue2);

			if(obj3 instanceof Integer)
			{
				jsonValue3 = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath3")));

			}
			else
				jsonValue3 = String.valueOf((String)response.jsonPath().get(map.get("JsonPath3")));

			testContextData.put(map.get("TestContextKeyName3"), jsonValue3);
			
			if(obj4 instanceof Integer)
			{
				jsonValue4 = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath4")));

			}
			else
				jsonValue4 = String.valueOf((String)response.jsonPath().get(map.get("JsonPath4")));

			testContextData.put(map.get("TestContextKeyName4"), jsonValue4);
		}
	}
	
	public static void storeJsonValueToTestContextForFourvariablesConcat(Response response, String storeJsonValueString) throws Exception
	{
		Map<String, String> map = RestUtils.jsonToMap(storeJsonValueString);

		if(map.containsKey("JsonPath") && map.containsKey("TestContextKeyName") && (map.containsKey("JsonPath1") && map.containsKey("TestContextKeyName1") && (map.containsKey("JsonPath2") && map.containsKey("TestContextKeyName2") && (map.containsKey("JsonPath3") && map.containsKey("TestContextKeyName3")))))
		{
			//System.out.println(response.body().asString());
			Object obj = response.jsonPath().get(map.get("JsonPath"));
			Object obj1 = response.jsonPath().get(map.get("JsonPath1"));
			int intobj = (Integer) obj1;
			Object obj2 = response.jsonPath().get(map.get("JsonPath2"));
			Object obj3 = response.jsonPath().get(map.get("JsonPath3"));
			String time = obj3.toString();
			String concat = time.concat(":00");

			testContextData.put(map.get("TestContextKeyName"), (String) obj);
			testContextData.put(map.get("TestContextKeyName1"), String.valueOf(intobj));
			testContextData.put(map.get("TestContextKeyName2"), (String) obj2);
			testContextData.put(map.get("TestContextKeyName3"), concat);
		}
	}

	public static void storeJsonValueToTestContextForFourvariables(Response response, String storeJsonValueString) throws Exception
	{
		Map<String, String> map = RestUtils.jsonToMap(storeJsonValueString);

		if(map.containsKey("JsonPath") && map.containsKey("TestContextKeyName") && (map.containsKey("JsonPath1") && map.containsKey("TestContextKeyName1") && (map.containsKey("JsonPath2") && map.containsKey("TestContextKeyName2") && (map.containsKey("JsonPath3") && map.containsKey("TestContextKeyName3")))))
		{
			//System.out.println(response.body().asString());
			Object obj = response.jsonPath().get(map.get("JsonPath"));
			Object obj1 = response.jsonPath().get(map.get("JsonPath1"));
			int intobj = (Integer) obj1;
			Object obj2 = response.jsonPath().get(map.get("JsonPath2"));
			Object obj3 = response.jsonPath().get(map.get("JsonPath3"));

			testContextData.put(map.get("TestContextKeyName"), (String) obj);
			testContextData.put(map.get("TestContextKeyName1"), String.valueOf(intobj));
			testContextData.put(map.get("TestContextKeyName2"), (String) obj2);
			testContextData.put(map.get("TestContextKeyName3"), (String) obj3);
		}
	}

	public static void storeJsonValueToTestContextForShiftTime(Response response, String storeJsonValueString) throws Exception
	{
		Map<String, String> map = RestUtils.jsonToMap(storeJsonValueString);

		if(map.containsKey("JsonPath") && map.containsKey("TestContextKeyName")||(map.containsKey("JsonPath1") && map.containsKey("TestContextKeyName1")))
		{
			Object obj = response.jsonPath().get(map.get("JsonPath"));
			//			System.out.println("Shifttime"+obj);
			testContextData.put(map.get("TestContextKeyName"), (String) obj);
		}
	}

	public static void storeJsonValueToTestContextForShiftTimetwovariablesInt(Response response, String storeJsonValueString) throws Exception
	{
		Map<String, String> map = RestUtils.jsonToMap(storeJsonValueString);

		if(map.containsKey("JsonPath") && map.containsKey("TestContextKeyName")&&(map.containsKey("JsonPath1") && map.containsKey("TestContextKeyName1")))
		{
			Object obj = response.jsonPath().get(map.get("JsonPath"));
			int intobj = (Integer) obj;
			Object obj1 = response.jsonPath().get(map.get("JsonPath1"));
			String jsonValue1 = "";
			//			System.out.println("Shifttime"+obj);
			testContextData.put(map.get("TestContextKeyName"), String.valueOf(intobj));
			System.out.println(testContextData.get(map.get("TestContextKeyName")));
			//		if(obj1 instanceof Integer)
			//		{
			//			jsonValue1 = String.valueOf((Integer)response.jsonPath().get(map.get("JsonPath1")));
			//
			//			}
			//			else
			//				jsonValue1 = String.valueOf(response.jsonPath().get(map.get("JsonPath1")));

			testContextData.put(map.get("TestContextKeyName1"),(String) obj1);
		}
	}
	public static void storeJsonValueToTestContextForShiftTimetwovariables(Response response, String storeJsonValueString) throws Exception
	{
//	    String temp="";
//		if (storeJsonValueString.contains("$$ShiftTimeUnder23Hour$$")) {
//			temp=storeJsonValueString.replace("$$ShiftTimeUnder23Hour$$",  Config.getBaseUrl(Config.PROPERTYFILEPATH,"ShiftTimeUnder23Hour"));
//		}
//		else {
//			temp=storeJsonValueString;}
		
		Map<String, String> map = RestUtils.jsonToMap(storeJsonValueString);
		String jsonValue1="";
		
		Object obj1 = response.jsonPath().get(map.get("JsonPath1"));

		if(map.containsKey("JsonPath") && map.containsKey("TestContextKeyName")&&(map.containsKey("JsonPath1") && map.containsKey("TestContextKeyName1")))
		{
			String jsonpath=map.get("JsonPath");
			Object obj=null;
			if (jsonpath.contains("$$")) {
			 obj = response.jsonPath().get(jsonpath.replace(jsonpath.substring(jsonpath.indexOf("$$"), jsonpath.lastIndexOf("$$")+2), Config.getBaseUrl(Config.PROPERTYFILEPATH,jsonpath.substring(jsonpath.indexOf("$$")+2, jsonpath.lastIndexOf("$$")))));
			}
			else {
				obj=response.jsonPath().get(map.get("JsonPath"));
			}
			String jsonPath1=map.get("JsonPath1");
			testContextData.put(map.get("TestContextKeyName"), (String) obj);
			if (jsonPath1.contains("$$")) {
			if(obj1 instanceof Integer)
			{
			
				jsonValue1 = String.valueOf((Integer)response.jsonPath().get(jsonPath1.replace(jsonPath1.substring(jsonPath1.indexOf("$$"), jsonPath1.lastIndexOf("$$")+2), Config.getBaseUrl(Config.PROPERTYFILEPATH,jsonPath1.substring(jsonPath1.indexOf("$$")+2, jsonPath1.lastIndexOf("$$"))))));

			}
			else {
				
				jsonValue1 = String.valueOf((Integer)response.jsonPath().get(jsonPath1.replace(jsonPath1.substring(jsonPath1.indexOf("$$"), jsonPath1.lastIndexOf("$$")+2), Config.getBaseUrl(Config.PROPERTYFILEPATH,jsonPath1.substring(jsonPath1.indexOf("$$")+2, jsonPath1.lastIndexOf("$$"))))));
			}
			}
			else {
				if(obj1 instanceof Integer)
				{
				
					jsonValue1 = String.valueOf((Integer)response.jsonPath().get(jsonPath1));

				}
				else {
					
					jsonValue1 = String.valueOf(response.jsonPath().get(jsonPath1));
				}
				
			}
			testContextData.put(map.get("TestContextKeyName1"), jsonValue1);

		}
	}

	public static String applyParametersToTemplateOrJsonPath(String templateOrJsonPathString
			, String parameterJsonString) throws Exception
	{

		if (templateOrJsonPathString != null && !templateOrJsonPathString.isEmpty() && parameterJsonString != null && !parameterJsonString.isEmpty()) {

			ObjectMapper mapper = new ObjectMapper();
			Map<String, String> map = new HashMap<String, String>();
			map = mapper.readValue(parameterJsonString, new TypeReference<Map<String, String>>() {
			});

			for(String key :map.keySet())
			{
				if(templateOrJsonPathString.contains("$$"+key+"$$"))
				{
					String value = map.get(key);
					if(value.startsWith("<<") && value.endsWith(">>"))
					{
						if(testContextData.containsKey(value.replace("<<", "").replace(">>", "")))
						{
							value = testContextData.get(value.replace("<<", "").replace(">>", ""));

						}

					}

					else if(value.startsWith("<GroovyScript>") && value.endsWith("</GroovyScript>"))
					{
						value = (String) Eval.me(value.replace("<GroovyScript>", "").replace("</GroovyScript>", ""));
					}
					else if(value.startsWith("$$") && value.endsWith("$$")) {
						
						value=Config.getBaseUrl(Config.PROPERTYFILEPATH,value.replace("$$",""));
						if(value.contains(":")) {
							value=value+":00";}
					}
					templateOrJsonPathString = templateOrJsonPathString.replace("$$"+key+"$$", value);
				}
			}
		}
		return templateOrJsonPathString;
	}

	@SuppressWarnings("unchecked")
	public static String checkCurrentDateWeekend(String templateOrJsonPathString) throws Exception
	{

		String temp="";
		if (templateOrJsonPathString != null ) {

			ObjectMapper mapper = new ObjectMapper();
			Map<String, String> map = new HashMap<String, String>();
			map = mapper.readValue(templateOrJsonPathString, HashMap.class);
			temp=templateOrJsonPathString;
			for(String key :map.keySet())
			{
				if(key.endsWith("Date"))
				{
					String value = map.get(key);
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					Date startDt=dateFormat.parse(value);
					Calendar c1 = Calendar.getInstance();
					c1.setTime(startDt);
					if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || 
							c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
						c1.add(Calendar.DATE, 3);
					}
					String startDate = dateFormat.format(c1.getTime());
					temp=templateOrJsonPathString.replace(value, startDate);
				}
				/*if(key.contains("endDate"))
				{
					String value = map.get(key);
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					Date endDt=dateFormat.parse(value);
					Calendar c1 = Calendar.getInstance();
					c1.setTime(endDt);
					if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || 
							c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
						c1.add(Calendar.DATE, 3);
					}
					String endDate = dateFormat.format(c1.getTime());
					temp=templateOrJsonPathString.replace(value, endDate);
				}*/
			}
		}
		return temp;
	}

	public static void Validateresponse(Response response, String dataID, String APIname, Map<String, String> testData)
	{
		testData = ExcelUtils.getInstance().getSheetAllDataRelatedToDataIDAndModule("TestData", dataID, APIname);

		String statusValidation = testData.get("ExpectedJsonResponseValue");
		String status = response.getBody().jsonPath().getString(testData.get("ExpectedJsonResponseKey"));
		String Successmsg=testData.get("ReportSuccessMessage");
		String Failuremessage=testData.get("ReportFailiureMessage");
		if (statusValidation.startsWith("$$")&&statusValidation.endsWith("$$")){
			String newStatusValidation=Config.getBaseUrl(Config.PROPERTYFILEPATH,statusValidation.replace("$$", ""));
			//if(status.contains(":"))
			//	status=status+":00";
			if (status.equalsIgnoreCase(newStatusValidation))
			{
				RestUtils.addReporterStepLog(Successmsg,
						"Value:"+status);
			}
			else
			{
				RestUtils.addReporterStepLogFailed(Failuremessage,
						". Expected status: "+newStatusValidation+". Actual status: "+status);
				Assert.fail("Unsuccessful. Expected status: "+newStatusValidation+". Actual status: "+status);
			}
			
		}
		else {
		if (status.equalsIgnoreCase(statusValidation))
		{
			RestUtils.addReporterStepLog(Successmsg,
					"Value:"+status);
		}
		else
		{
			RestUtils.addReporterStepLogFailed(Failuremessage,
					". Expected status: "+statusValidation+". Actual status: "+status);
			Assert.fail("Unsuccessful. Expected status: "+statusValidation+". Actual status: "+status);
		}
		}

	}
}
