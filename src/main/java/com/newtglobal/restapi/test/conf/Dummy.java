package com.newtglobal.restapi.test.conf;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import org.testng.TestNG;

import java.net.URI;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import groovy.util.Eval;

public class Dummy {
	public static void main(String[] arg) {
		//      Object str = RestAssured.given().get("http://dummy.restapiexample.com/api/v1/employees").getBody().jsonPath()
		//              .get("findAll{it.id=='1675'}.employee_name");
		//      System.out.println(str);

//		System.out.println(Eval.me("(new Date()).format('M')"));

		String s = "[\r\n" + 
				"  {\r\n" + 
				"    \"escortRequired\": \"N\",\r\n" + 
				"    \"vendorId\": 5,\r\n" + 
				"    \"suggestiveVehicleNumber\": \"No\",\r\n" + 
				"    \"waypoints\": \"\",\r\n" + 
				"    \"deviceNumber\": 1,\r\n" + 
				"    \"dropPickAreaName\": \"MUHANA MANDI - DADU DAYAL NAGAR\",\r\n" + 
				"    \"capacity\": 7,\r\n" + 
				"    \"routeName\": \"MUHANA MANDI - MUHANA MANDI ROAD\",\r\n" + 
				"    \"shiftTime\": \"15:15\",\r\n" + 
				"    \"driverNumber\": \"910000000000\",\r\n" + 
				"    \"vehicleAvailableCapacity\": 5,\r\n" + 
				"    \"routeZoneName\": \"ZONE JAI\",\r\n" + 
				"    \"routeId\": 841675,\r\n" + 
				"    \"transportNumber\": \"919003445653\",\r\n" + 
				"    \"bucketStatus\": \"N\",\r\n" + 
				"    \"vehicleNumber\": \"NA\",\r\n" + 
				"    \"zoneRouteId\": 7,\r\n" + 
				"    \"vehicleId\": 10435,\r\n" + 
				"    \"routeType\": \"normal\",\r\n" + 
				"    \"vehicleType\": \"Tempo Traveller\",\r\n" + 
				"    \"isBackTwoBack\": \"N\",\r\n" + 
				"    \"tripActualAssignDate\": \"07-11-2019\",\r\n" + 
				"    \"routeZoneId\": 7,\r\n" + 
				"    \"startPageNo\": 0,\r\n" + 
				"    \"escortName\": \"Not Required\",\r\n" + 
				"    \"empDetails\": [],\r\n" + 
				"    \"vendorName\": \"NA\",\r\n" + 
				"    \"numberOfEmployees\": 1,\r\n" + 
				"    \"totalRecordCount\": 1,\r\n" + 
				"    \"baseLatLong\": \"26.806328,75.643640\",\r\n" + 
				"    \"tripType\": \"DROP\",\r\n" + 
				"    \"checkInId\": 1256,\r\n" + 
				"    \"driverId\": 11527,\r\n" + 
				"    \"driverName\": \"NA\",\r\n" + 
				"    \"assignRouteId\": 841675,\r\n" + 
				"    \"vehicleStatus\": \"A\",\r\n" + 
				"    \"tripStatus\": \"allocated\"\r\n" + 
				"  }\r\n" + 
				"]";

		//Object pstr = new JsonPath(s).get("..[?(@.shiftTime=='01:00')].shiftId");
		//Object pstr = new JsonPath(s).get("centerWiseList.findAll{it.tripType=='PICKUP")[0];
//		Object pstr = new JsonPath(s).get("shift.shiftTime[1]");
		//Object pstr = new JsonPath(s).get("findAll{it.routeName=='ZONE DEL'}");
		//Object pstr = new JsonPath(s).get("shift.findAll{it.shiftTime=='01:00'}[0].shiftTime");
		//Object pstr = new JsonPath(s).get("findAll{it.projectName=='Reddaway - Bill Entry (J)'}.facilityName[1]");
		Object pstr = new JsonPath(s).get("tripStatus[0]");
		System.out.println(pstr);
		
		/*String str = pstr.toString();
		
		String vehicleNo[] = str.split("\\(");
		
		System.out.println("Vehicle No: "+vehicleNo[0]);
		
		System.out.println("Sticker No: "+vehicleNo[1].replace(")", ""));*/
	}
}
