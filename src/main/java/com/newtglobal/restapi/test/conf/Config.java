package com.newtglobal.restapi.test.conf;

import org.apache.commons.io.FileUtils;

import org.apache.commons.io.FilenameUtils;
import com.newtglobal.restapi.test.utils.ExcelUtils;
import org.testng.Assert;

import com.newtglobal.restapi.test.utils.ExcelUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

public class Config {

    public static final String TIMESTAMP = String.valueOf(new Date().getTime());
    public static  String PROPERTYFILEPATH = "./Objects/EnvironmentDetails.properties";
    public static final String BASEURL =  getBaseUrl(PROPERTYFILEPATH, "BaseURL");
    public static final String EXCELPATH =  getBaseUrl(PROPERTYFILEPATH, "ExcelPath");
    public static final Map<String, String> TEMPLATES = getTemplates();

    private static synchronized Map<String, String> getTemplates() {
    	
    	
        Map<String, String> ret = new LinkedHashMap<>();
        try {
            Collection<File> files = FileUtils.listFiles(new File("./Templates/"), new String[]{"txt"}, false);
            for (File file : files)
                ret.put(FilenameUtils.getBaseName(file.getName()), FileUtils.readFileToString(file, Charset.defaultCharset()));
        } catch (Exception e) {
            Assert.fail(e.toString());
        }
        return ret;
    }
    
    public static String getBaseUrl(String filename ,String propertyKey)
    {
    	PropertyUtils prop = new PropertyUtils();
    	prop.PropertyUtility(filename);
    	String getproperty = prop.getProperty(propertyKey);
    	//System.out.println(propertyKey+"="+getproperty);
    	return getproperty;
    }
}
