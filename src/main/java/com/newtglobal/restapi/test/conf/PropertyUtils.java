package com.newtglobal.restapi.test.conf;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtils {
	
	Properties prop = new Properties();
	
	
	public PropertyUtils()
	{
		
	}
	
	public void PropertyUtility(String fileName)
	{
		try{
			InputStream input =  null;
			input = new FileInputStream(fileName);
			prop.load(input);
		}
		catch (IOException e) {
//			LOGGER.info("error:  " +e);
			e.printStackTrace();
		}		
	}
	
	public String getProperty(String propertyKey){
		String propertyValue = "";
		propertyValue=prop.getProperty(propertyKey);
		return propertyValue;
	}

}
