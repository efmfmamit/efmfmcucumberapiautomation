$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("./features/RoutingCabAllocation.feature");
formatter.feature({
  "line": 1,
  "name": "Routing Cab allocation",
  "description": "",
  "id": "routing-cab-allocation",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 2,
      "value": "#Scenario Outline: Ability to perform advance search with one facility and also with future date"
    },
    {
      "line": 3,
      "value": "#Given Service end point is active"
    },
    {
      "line": 4,
      "value": "#Then Do Login"
    },
    {
      "line": 5,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 6,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 7,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 8,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 9,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 10,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 11,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 12,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 13,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 14,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 15,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 16,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 17,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 18,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 19,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 20,
      "value": "#Examples:"
    },
    {
      "line": 21,
      "value": "#|TestCaseID|"
    },
    {
      "line": 22,
      "value": "#|TC_001|"
    },
    {
      "line": 24,
      "value": "#Scenario Outline: Ability to perform advance search with multiple facility"
    },
    {
      "line": 25,
      "value": "#Given Service end point is active"
    },
    {
      "line": 26,
      "value": "#Then Do Login"
    },
    {
      "line": 27,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 28,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 29,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 30,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 31,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 32,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 33,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 34,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 35,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 36,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 37,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 38,
      "value": "#Then View Routing Details based on multiple facilities for_\u003cTestCaseID\u003e"
    },
    {
      "line": 39,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 40,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 41,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 42,
      "value": "#Examples:"
    },
    {
      "line": 43,
      "value": "#|TestCaseID|"
    },
    {
      "line": 44,
      "value": "#|TC_001|"
    },
    {
      "line": 46,
      "value": "#Scenario Outline: Ability to perform advance search with normal trip type"
    },
    {
      "line": 47,
      "value": "#Given Service end point is active"
    },
    {
      "line": 48,
      "value": "#Then Do Login"
    },
    {
      "line": 49,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 50,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 51,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 52,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 53,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 54,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 55,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 56,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 57,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 58,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 59,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 60,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 61,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 62,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 63,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 64,
      "value": "#Examples:"
    },
    {
      "line": 65,
      "value": "#|TestCaseID|"
    },
    {
      "line": 66,
      "value": "#|TC_001|"
    },
    {
      "line": 68,
      "value": "#Scenario Outline: Ability to perform advance search based on adhoc trip type and shift time"
    },
    {
      "line": 69,
      "value": "#Given Service end point is active"
    },
    {
      "line": 70,
      "value": "#Then Do Login"
    },
    {
      "line": 71,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 72,
      "value": "#Then Create adhoc request self for_\u003cTestCaseID\u003e"
    },
    {
      "line": 73,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 74,
      "value": "#Then Allocate Adhoc cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 75,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 76,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 77,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 78,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 79,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 80,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 81,
      "value": "#Examples:"
    },
    {
      "line": 82,
      "value": "#|TestCaseID|"
    },
    {
      "line": 83,
      "value": "#|TC_001|"
    },
    {
      "line": 85,
      "value": "#Scenario Outline: Ability to perform normal search for upcoming trips with route id"
    },
    {
      "line": 86,
      "value": "#Given Service end point is active"
    },
    {
      "line": 87,
      "value": "#Then Do Login"
    },
    {
      "line": 88,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 89,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 90,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 91,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 92,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 93,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 94,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 95,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 96,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 97,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 98,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 99,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 100,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 101,
      "value": "#Then Search by route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 102,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 103,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 104,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 105,
      "value": "#Examples:"
    },
    {
      "line": 106,
      "value": "#|TestCaseID|"
    },
    {
      "line": 107,
      "value": "#|TC_001|"
    },
    {
      "line": 109,
      "value": "#Scenario Outline: Ability to perform normal search with invalid route id"
    },
    {
      "line": 110,
      "value": "#Given Service end point is active"
    },
    {
      "line": 111,
      "value": "#Then Do Login"
    },
    {
      "line": 112,
      "value": "#Then Search by invalid route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 113,
      "value": "#Examples:"
    },
    {
      "line": 114,
      "value": "#|TestCaseID|"
    },
    {
      "line": 115,
      "value": "#|TC_001|"
    },
    {
      "line": 117,
      "value": "#Scenario Outline: Ability to perform normal search for incompleted trips with route id"
    },
    {
      "line": 118,
      "value": "#Given Service end point is active"
    },
    {
      "line": 119,
      "value": "#Then Do Login"
    },
    {
      "line": 120,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 121,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 122,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 123,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 124,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 125,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 126,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 127,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 128,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 129,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 130,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 131,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 132,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 133,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 134,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 135,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 136,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 137,
      "value": "#Then Manually start the trip for_\u003cTestCaseID\u003e"
    },
    {
      "line": 138,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 139,
      "value": "#Then Update employee status for_\u003cTestCaseID\u003e"
    },
    {
      "line": 140,
      "value": "#Then Search by route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 141,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 142,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 143,
      "value": "#Examples:"
    },
    {
      "line": 144,
      "value": "#|TestCaseID|"
    },
    {
      "line": 145,
      "value": "#|TC_001|"
    },
    {
      "line": 147,
      "value": "#Scenario Outline: Ability to perform normal search for upcoming trips with vehicle number"
    },
    {
      "line": 148,
      "value": "#Given Service end point is active"
    },
    {
      "line": 149,
      "value": "#Then Do Login"
    },
    {
      "line": 150,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 151,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 152,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 153,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 154,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 155,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 156,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 157,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 158,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 159,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 160,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 161,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 162,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 163,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 164,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 165,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 166,
      "value": "#Then Search by vehicle number for_\u003cTestCaseID\u003e"
    },
    {
      "line": 167,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 168,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 169,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 170,
      "value": "#Examples:"
    },
    {
      "line": 171,
      "value": "#|TestCaseID|"
    },
    {
      "line": 172,
      "value": "#|TC_001|"
    },
    {
      "line": 174,
      "value": "#Scenario Outline: Ability to perform normal search for unapproved vehicle number of base facility"
    },
    {
      "line": 175,
      "value": "#Given Service end point is active"
    },
    {
      "line": 176,
      "value": "#Then Do Login"
    },
    {
      "line": 177,
      "value": "#Then Add new vehicle for base facility_\u003cTestCaseID\u003e"
    },
    {
      "line": 178,
      "value": "#Then Search unapproved vehicle for_\u003cTestCaseID\u003e"
    },
    {
      "line": 179,
      "value": "#Then Remove vehicle for_\u003cTestCaseID\u003e"
    },
    {
      "line": 180,
      "value": "#Examples:"
    },
    {
      "line": 181,
      "value": "#|TestCaseID|"
    },
    {
      "line": 182,
      "value": "#|TC_001|"
    },
    {
      "line": 184,
      "value": "#Scenario Outline: Ability to perform normal search for unapproved vehicle number of other facility"
    },
    {
      "line": 185,
      "value": "#Given Service end point is active"
    },
    {
      "line": 186,
      "value": "#Then Do Login"
    },
    {
      "line": 187,
      "value": "#Then Add new vehicle for other facility_\u003cTestCaseID\u003e"
    },
    {
      "line": 188,
      "value": "#Then Search unapproved vehicle for_\u003cTestCaseID\u003e"
    },
    {
      "line": 189,
      "value": "#Then Remove vehicle for_\u003cTestCaseID\u003e"
    },
    {
      "line": 190,
      "value": "#Examples:"
    },
    {
      "line": 191,
      "value": "#|TestCaseID|"
    },
    {
      "line": 192,
      "value": "#|TC_001|"
    },
    {
      "line": 194,
      "value": "#Scenario Outline: Ability to perform normal search for upcoming trips with sticker number"
    },
    {
      "line": 195,
      "value": "#Given Service end point is active"
    },
    {
      "line": 196,
      "value": "#Then Do Login"
    },
    {
      "line": 197,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 198,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 199,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 200,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 201,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 202,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 203,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 204,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 205,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 206,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 207,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 208,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 209,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 210,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 211,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 212,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 213,
      "value": "#Then Search by sticker number for_\u003cTestCaseID\u003e"
    },
    {
      "line": 214,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 215,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 216,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 217,
      "value": "#Examples:"
    },
    {
      "line": 218,
      "value": "#|TestCaseID|"
    },
    {
      "line": 219,
      "value": "#|TC_001|"
    },
    {
      "line": 221,
      "value": "#Scenario Outline: Ability to perform normal search for unapproved sticker number of base facility"
    },
    {
      "line": 222,
      "value": "#Given Service end point is active"
    },
    {
      "line": 223,
      "value": "#Then Do Login"
    },
    {
      "line": 224,
      "value": "#Then Add new vehicle for base facility_\u003cTestCaseID\u003e"
    },
    {
      "line": 225,
      "value": "#Then Get unapproved vehicle list for base facility_\u003cTestCaseID\u003e"
    },
    {
      "line": 226,
      "value": "#Then Search by sticker number for_\u003cTestCaseID\u003e"
    },
    {
      "line": 227,
      "value": "#Then Remove vehicle for_\u003cTestCaseID\u003e"
    },
    {
      "line": 228,
      "value": "#Examples:"
    },
    {
      "line": 229,
      "value": "#|TestCaseID|"
    },
    {
      "line": 230,
      "value": "#|TC_001|"
    },
    {
      "line": 232,
      "value": "#Scenario Outline: Ability to perform normal search for unapproved sticker number of other facility"
    },
    {
      "line": 233,
      "value": "#Given Service end point is active"
    },
    {
      "line": 234,
      "value": "#Then Do Login"
    },
    {
      "line": 235,
      "value": "#Then Add new vehicle for other facility_\u003cTestCaseID\u003e"
    },
    {
      "line": 236,
      "value": "#Then Get unapproved vehicle list for other facility_\u003cTestCaseID\u003e"
    },
    {
      "line": 237,
      "value": "#Then Search by sticker number for_\u003cTestCaseID\u003e"
    },
    {
      "line": 238,
      "value": "#Then Remove vehicle for_\u003cTestCaseID\u003e"
    },
    {
      "line": 239,
      "value": "#Examples:"
    },
    {
      "line": 240,
      "value": "#|TestCaseID|"
    },
    {
      "line": 241,
      "value": "#|TC_001|"
    },
    {
      "line": 243,
      "value": "#Scenario Outline: Ability to perform normal search for upcoming trips with employee id"
    },
    {
      "line": 244,
      "value": "#Given Service end point is active"
    },
    {
      "line": 245,
      "value": "#Then Do Login"
    },
    {
      "line": 246,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 247,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 248,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 249,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 250,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 251,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 252,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 253,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 254,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 255,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 256,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 257,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 258,
      "value": "#Then Search by employee id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 259,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 260,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 261,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 262,
      "value": "#Examples:"
    },
    {
      "line": 263,
      "value": "#|TestCaseID|"
    },
    {
      "line": 264,
      "value": "#|TC_001|"
    },
    {
      "line": 266,
      "value": "#Scenario Outline: Ability to perform normal search with invalid employee id"
    },
    {
      "line": 267,
      "value": "#Given Service end point is active"
    },
    {
      "line": 268,
      "value": "#Then Do Login"
    },
    {
      "line": 269,
      "value": "#Then Search by invalid employee id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 270,
      "value": "#Examples:"
    },
    {
      "line": 271,
      "value": "#|TestCaseID|"
    },
    {
      "line": 272,
      "value": "#|TC_001|"
    },
    {
      "line": 274,
      "value": "#Scenario Outline: Ability to perform normal search by employee id with route id"
    },
    {
      "line": 275,
      "value": "#Given Service end point is active"
    },
    {
      "line": 276,
      "value": "#Then Do Login"
    },
    {
      "line": 277,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 278,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 279,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 280,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 281,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 282,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 283,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 284,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 285,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 286,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 287,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 288,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 289,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 290,
      "value": "#Then Search by employee id with valid route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 291,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 292,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 293,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 294,
      "value": "#Examples:"
    },
    {
      "line": 295,
      "value": "#|TestCaseID|"
    },
    {
      "line": 296,
      "value": "#|TC_001|"
    },
    {
      "line": 298,
      "value": "#Scenario Outline: Ability to verify number of zone details"
    },
    {
      "line": 299,
      "value": "#Given Service end point is active"
    },
    {
      "line": 300,
      "value": "#Then Do Login"
    },
    {
      "line": 301,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 302,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 303,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 304,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 305,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 306,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 307,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 308,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 309,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 310,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 311,
      "value": "#Then Validate total number of zone details_\u003cTestCaseID\u003e"
    },
    {
      "line": 312,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 313,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 314,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 315,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 316,
      "value": "#Examples:"
    },
    {
      "line": 317,
      "value": "#|TestCaseID|"
    },
    {
      "line": 318,
      "value": "#|TC_001|"
    },
    {
      "line": 320,
      "value": "#Scenario Outline: Ability to verify number of route details"
    },
    {
      "line": 321,
      "value": "#Given Service end point is active"
    },
    {
      "line": 322,
      "value": "#Then Do Login"
    },
    {
      "line": 323,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 324,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 325,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 326,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 327,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 328,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 329,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 330,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 331,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 332,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 333,
      "value": "#Then Validate total number of route details_\u003cTestCaseID\u003e"
    },
    {
      "line": 334,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 335,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 336,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 337,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 338,
      "value": "#Examples:"
    },
    {
      "line": 339,
      "value": "#|TestCaseID|"
    },
    {
      "line": 340,
      "value": "#|TC_001|"
    },
    {
      "line": 342,
      "value": "#Scenario Outline: Ability to verify allocated vendor name"
    },
    {
      "line": 343,
      "value": "#Given Service end point is active"
    },
    {
      "line": 344,
      "value": "#Then Do Login"
    },
    {
      "line": 345,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 346,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 347,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 348,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 349,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 350,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 351,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 352,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 353,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 354,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 355,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 356,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 357,
      "value": "#Then Allocate Vendor in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 358,
      "value": "#Then Validate vendor name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 359,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 360,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 361,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 362,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 363,
      "value": "#Examples:"
    },
    {
      "line": 364,
      "value": "#|TestCaseID|"
    },
    {
      "line": 365,
      "value": "#|TC_001|"
    },
    {
      "line": 367,
      "value": "#Scenario Outline: Ability to verify allocated seating capacity"
    },
    {
      "line": 368,
      "value": "#Given Service end point is active"
    },
    {
      "line": 369,
      "value": "#Then Do Login"
    },
    {
      "line": 370,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 371,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 372,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 373,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 374,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 375,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 376,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 377,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 378,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 379,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 380,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 381,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 382,
      "value": "#Then Allocate Vendor in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 383,
      "value": "#Then Validate seating capacity for_\u003cTestCaseID\u003e"
    },
    {
      "line": 384,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 385,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 386,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 387,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 388,
      "value": "#Examples:"
    },
    {
      "line": 389,
      "value": "#|TestCaseID|"
    },
    {
      "line": 390,
      "value": "#|TC_001|"
    },
    {
      "line": 392,
      "value": "#Scenario Outline: Ability to verify allocated trip count"
    },
    {
      "line": 393,
      "value": "#Given Service end point is active"
    },
    {
      "line": 394,
      "value": "#Then Do Login"
    },
    {
      "line": 395,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 396,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 397,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 398,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 399,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 400,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 401,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 402,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 403,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 404,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 405,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 406,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 407,
      "value": "#Then Allocate Vendor in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 408,
      "value": "#Then Validate trip count for_\u003cTestCaseID\u003e"
    },
    {
      "line": 409,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 410,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 411,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 412,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 413,
      "value": "#Examples:"
    },
    {
      "line": 414,
      "value": "#|TestCaseID|"
    },
    {
      "line": 415,
      "value": "#|TC_001|"
    },
    {
      "line": 417,
      "value": "#Scenario Outline: Ability to verify number of open route details"
    },
    {
      "line": 418,
      "value": "#Given Service end point is active"
    },
    {
      "line": 419,
      "value": "#Then Do Login"
    },
    {
      "line": 420,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 421,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 422,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 423,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 424,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 425,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 426,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 427,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 428,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 429,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 430,
      "value": "#Then Validate total number of open route details_\u003cTestCaseID\u003e"
    },
    {
      "line": 431,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 432,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 433,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 434,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 435,
      "value": "#Examples:"
    },
    {
      "line": 436,
      "value": "#|TestCaseID|"
    },
    {
      "line": 437,
      "value": "#|TC_001|"
    },
    {
      "line": 439,
      "value": "#Scenario Outline: Ability to verify open vendor name"
    },
    {
      "line": 440,
      "value": "#Given Service end point is active"
    },
    {
      "line": 441,
      "value": "#Then Do Login"
    },
    {
      "line": 442,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 443,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 444,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 445,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 446,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 447,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 448,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 449,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 450,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 451,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 452,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 453,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 454,
      "value": "#Then Allocate Vendor in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 455,
      "value": "#Then Validate open vendor name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 456,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 457,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 458,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 459,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 460,
      "value": "#Examples:"
    },
    {
      "line": 461,
      "value": "#|TestCaseID|"
    },
    {
      "line": 462,
      "value": "#|TC_001|"
    },
    {
      "line": 464,
      "value": "#Scenario Outline: Ability to verify open vendor seating capacity"
    },
    {
      "line": 465,
      "value": "#Given Service end point is active"
    },
    {
      "line": 466,
      "value": "#Then Do Login"
    },
    {
      "line": 467,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 468,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 469,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 470,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 471,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 472,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 473,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 474,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 475,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 476,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 477,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 478,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 479,
      "value": "#Then Allocate Vendor in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 480,
      "value": "#Then Validate open vendor seating capacity for_\u003cTestCaseID\u003e"
    },
    {
      "line": 481,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 482,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 483,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 484,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 485,
      "value": "#Examples:"
    },
    {
      "line": 486,
      "value": "#|TestCaseID|"
    },
    {
      "line": 487,
      "value": "#|TC_001|"
    },
    {
      "line": 489,
      "value": "#Scenario Outline: Ability to verify open vendor trip count"
    },
    {
      "line": 490,
      "value": "#Given Service end point is active"
    },
    {
      "line": 491,
      "value": "#Then Do Login"
    },
    {
      "line": 492,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 493,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 494,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 495,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 496,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 497,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 498,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 499,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 500,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 501,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 502,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 503,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 504,
      "value": "#Then Allocate Vendor in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 505,
      "value": "#Then Validate open vendor trip count for_\u003cTestCaseID\u003e"
    },
    {
      "line": 506,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 507,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 508,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 509,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 510,
      "value": "#Examples:"
    },
    {
      "line": 511,
      "value": "#|TestCaseID|"
    },
    {
      "line": 512,
      "value": "#|TC_001|"
    },
    {
      "line": 514,
      "value": "#Scenario Outline: Ability to verify count of closed routes"
    },
    {
      "line": 515,
      "value": "#Given Service end point is active"
    },
    {
      "line": 516,
      "value": "#Then Do Login"
    },
    {
      "line": 517,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 518,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 519,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 520,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 521,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 522,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 523,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 524,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 525,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 526,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 527,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 528,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 529,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 530,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 531,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 532,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 533,
      "value": "#Then Validate count of closed routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 534,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 535,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 536,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 537,
      "value": "#Examples:"
    },
    {
      "line": 538,
      "value": "#|TestCaseID|"
    },
    {
      "line": 539,
      "value": "#|TC_001|"
    },
    {
      "line": 541,
      "value": "#Scenario Outline: Ability to verify count of started routes"
    },
    {
      "line": 542,
      "value": "#Given Service end point is active"
    },
    {
      "line": 543,
      "value": "#Then Do Login"
    },
    {
      "line": 544,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 545,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 546,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 547,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 548,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 549,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 550,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 551,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 552,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 553,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 554,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 555,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 556,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 557,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 558,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 559,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 560,
      "value": "#Then Manually start the trip for_\u003cTestCaseID\u003e"
    },
    {
      "line": 561,
      "value": "#Then Validate count of started routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 562,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 563,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 564,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 565,
      "value": "#Examples:"
    },
    {
      "line": 566,
      "value": "#|TestCaseID|"
    },
    {
      "line": 567,
      "value": "#|TC_001|"
    },
    {
      "line": 569,
      "value": "#Scenario Outline: Ability to verify count of closed but not started routes"
    },
    {
      "line": 570,
      "value": "#Given Service end point is active"
    },
    {
      "line": 571,
      "value": "#Then Do Login"
    },
    {
      "line": 572,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 573,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 574,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 575,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 576,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 577,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 578,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 579,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 580,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 581,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 582,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 583,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 584,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 585,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 586,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 587,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 588,
      "value": "#Then Validate count of closed but not started routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 589,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 590,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 591,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 592,
      "value": "#Examples:"
    },
    {
      "line": 593,
      "value": "#|TestCaseID|"
    },
    {
      "line": 594,
      "value": "#|TC_001|"
    },
    {
      "line": 596,
      "value": "#Scenario Outline: Ability to verify count of Required Escorts"
    },
    {
      "line": 597,
      "value": "#Given Service end point is active"
    },
    {
      "line": 598,
      "value": "#Then Do Login"
    },
    {
      "line": 599,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 600,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 601,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 602,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 603,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 604,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 605,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 606,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 607,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 608,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 609,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 610,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 611,
      "value": "#Then Get Escort checkIn Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 612,
      "value": "#Then Add Escort for_\u003cTestCaseID\u003e"
    },
    {
      "line": 613,
      "value": "#Then Validate count of Required Escorts for_\u003cTestCaseID\u003e"
    },
    {
      "line": 614,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 615,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 616,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 617,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 618,
      "value": "#Examples:"
    },
    {
      "line": 619,
      "value": "#|TestCaseID|"
    },
    {
      "line": 620,
      "value": "#|TC_001|"
    },
    {
      "line": 622,
      "value": "#Scenario Outline: Ability to verify count of employees yet to onboard"
    },
    {
      "line": 623,
      "value": "#Given Service end point is active"
    },
    {
      "line": 624,
      "value": "#Then Do Login"
    },
    {
      "line": 625,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 626,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 627,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 628,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 629,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 630,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 631,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 632,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 633,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 634,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 635,
      "value": "#Then Validate count of employees yet to onboard for_\u003cTestCaseID\u003e"
    },
    {
      "line": 636,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 637,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 638,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 639,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 640,
      "value": "#Examples:"
    },
    {
      "line": 641,
      "value": "#|TestCaseID|"
    },
    {
      "line": 642,
      "value": "#|TC_001|"
    },
    {
      "line": 644,
      "value": "#Scenario Outline: Ability to verify vehicle occupancy percentage"
    },
    {
      "line": 645,
      "value": "#Given Service end point is active"
    },
    {
      "line": 646,
      "value": "#Then Do Login"
    },
    {
      "line": 647,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 648,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 649,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 650,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 651,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 652,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 653,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 654,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 655,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 656,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 657,
      "value": "#Then Validate Vehicle Occupancy percentage for_\u003cTestCaseID\u003e"
    },
    {
      "line": 658,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 659,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 660,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 661,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 662,
      "value": "#Examples:"
    },
    {
      "line": 663,
      "value": "#|TestCaseID|"
    },
    {
      "line": 664,
      "value": "#|TC_001|"
    },
    {
      "line": 666,
      "value": "#Scenario Outline: Ability to verify count of total employees"
    },
    {
      "line": 667,
      "value": "#Given Service end point is active"
    },
    {
      "line": 668,
      "value": "#Then Do Login"
    },
    {
      "line": 669,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 670,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 671,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 672,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 673,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 674,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 675,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 676,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 677,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 678,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 679,
      "value": "#Then Validate Total Employee Count for_\u003cTestCaseID\u003e"
    },
    {
      "line": 680,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 681,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 682,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 683,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 684,
      "value": "#Examples:"
    },
    {
      "line": 685,
      "value": "#|TestCaseID|"
    },
    {
      "line": 686,
      "value": "#|TC_001|"
    },
    {
      "line": 688,
      "value": "#Scenario Outline: Ability to verify count of total male employees"
    },
    {
      "line": 689,
      "value": "#Given Service end point is active"
    },
    {
      "line": 690,
      "value": "#Then Do Login"
    },
    {
      "line": 691,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 692,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 693,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 694,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 695,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 696,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 697,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 698,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 699,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 700,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 701,
      "value": "#Then Validate Total Male Employee Count for_\u003cTestCaseID\u003e"
    },
    {
      "line": 702,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 703,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 704,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 705,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 706,
      "value": "#Examples:"
    },
    {
      "line": 707,
      "value": "#|TestCaseID|"
    },
    {
      "line": 708,
      "value": "#|TC_001|"
    },
    {
      "line": 710,
      "value": "#Scenario Outline: Ability to verify count of total female employees"
    },
    {
      "line": 711,
      "value": "#Given Service end point is active"
    },
    {
      "line": 712,
      "value": "#Then Do Login"
    },
    {
      "line": 713,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 714,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 715,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 716,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 717,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 718,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 719,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 720,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 721,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 722,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 723,
      "value": "#Then Validate Total female Employee Count for_\u003cTestCaseID\u003e"
    },
    {
      "line": 724,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 725,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 726,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 727,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 728,
      "value": "#Examples:"
    },
    {
      "line": 729,
      "value": "#|TestCaseID|"
    },
    {
      "line": 730,
      "value": "#|TC_001|"
    },
    {
      "line": 732,
      "value": "#Scenario Outline: Ability to verify capacity count of vehicle"
    },
    {
      "line": 733,
      "value": "#Given Service end point is active"
    },
    {
      "line": 734,
      "value": "#Then Do Login"
    },
    {
      "line": 735,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 736,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 737,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 738,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 739,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 740,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 741,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 742,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 743,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 744,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 745,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 746,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 747,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 748,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 749,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 750,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 751,
      "value": "#Then Validate Capacity count of vehicle for_\u003cTestCaseID\u003e"
    },
    {
      "line": 752,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 753,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 754,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 755,
      "value": "#Examples:"
    },
    {
      "line": 756,
      "value": "#|TestCaseID|"
    },
    {
      "line": 757,
      "value": "#|TC_001|"
    },
    {
      "line": 759,
      "value": "#Scenario Outline: Ability to verify capacity of vehicle"
    },
    {
      "line": 760,
      "value": "#Given Service end point is active"
    },
    {
      "line": 761,
      "value": "#Then Do Login"
    },
    {
      "line": 762,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 763,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 764,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 765,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 766,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 767,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 768,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 769,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 770,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 771,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 772,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 773,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 774,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 775,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 776,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 777,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 778,
      "value": "#Then Validate Capacity of vehicle for_\u003cTestCaseID\u003e"
    },
    {
      "line": 779,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 780,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 781,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 782,
      "value": "#Examples:"
    },
    {
      "line": 783,
      "value": "#|TestCaseID|"
    },
    {
      "line": 784,
      "value": "#|TC_001|"
    },
    {
      "line": 786,
      "value": "#Scenario Outline: Ability to verify route details in particular zone"
    },
    {
      "line": 787,
      "value": "#Given Service end point is active"
    },
    {
      "line": 788,
      "value": "#Then Do Login"
    },
    {
      "line": 789,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 790,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 791,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 792,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 793,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 794,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 795,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 796,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 797,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 798,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 799,
      "value": "#Then Validate Number of routes in a particular zone_\u003cTestCaseID\u003e"
    },
    {
      "line": 800,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 801,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 802,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 803,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 804,
      "value": "#Examples:"
    },
    {
      "line": 805,
      "value": "#|TestCaseID|"
    },
    {
      "line": 806,
      "value": "#|TC_001|"
    },
    {
      "line": 808,
      "value": "#Scenario Outline: Ability to verify edit bucket before adding billing cycle"
    },
    {
      "line": 809,
      "value": "#Given Service end point is active"
    },
    {
      "line": 810,
      "value": "#Then Do Login"
    },
    {
      "line": 811,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 812,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 813,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 814,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 815,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 816,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 817,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 818,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 819,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 820,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 821,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 822,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 823,
      "value": "#Then Get freezecost id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 824,
      "value": "#Then Close freezecost for_\u003cTestCaseID\u003e"
    },
    {
      "line": 825,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 826,
      "value": "#Then Add vehicle to route before billing cycle for_\u003cTestCaseID\u003e"
    },
    {
      "line": 827,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 828,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 829,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 830,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 831,
      "value": "#Examples:"
    },
    {
      "line": 832,
      "value": "#|TestCaseID|"
    },
    {
      "line": 833,
      "value": "#|TC_001|"
    },
    {
      "line": 835,
      "value": "#Scenario Outline: Ability to verify edit bucket after adding billing cycle"
    },
    {
      "line": 836,
      "value": "#Given Service end point is active"
    },
    {
      "line": 837,
      "value": "#Then Do Login"
    },
    {
      "line": 838,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 839,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 840,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 841,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 842,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 843,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 844,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 845,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 846,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 847,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 848,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 849,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 850,
      "value": "#Then Get freezecost id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 851,
      "value": "#Then Open freezecost for_\u003cTestCaseID\u003e"
    },
    {
      "line": 852,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 853,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 854,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 855,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 856,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 857,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 858,
      "value": "#Examples:"
    },
    {
      "line": 859,
      "value": "#|TestCaseID|"
    },
    {
      "line": 860,
      "value": "#|TC_001|"
    },
    {
      "line": 862,
      "value": "#Scenario Outline: Ability to verify route edit bucket with same driver for more than one route"
    },
    {
      "line": 863,
      "value": "#Given Service end point is active"
    },
    {
      "line": 864,
      "value": "#Then Do Login"
    },
    {
      "line": 865,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 866,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 867,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 868,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 869,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 870,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 871,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 872,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 873,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 874,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 875,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 876,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 877,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 878,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 879,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 880,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 881,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 882,
      "value": "#Then Create new route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 883,
      "value": "#Then Add vehicle to new route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 884,
      "value": "#Then Close the route with duplicate details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 885,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 886,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 887,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 888,
      "value": "#Examples:"
    },
    {
      "line": 889,
      "value": "#|TestCaseID|"
    },
    {
      "line": 890,
      "value": "#|TC_001|"
    },
    {
      "line": 892,
      "value": "#Scenario Outline: Ability to verify escort mandatory when female employee in trip"
    },
    {
      "line": 893,
      "value": "#Given Service end point is active"
    },
    {
      "line": 894,
      "value": "#Then Do Login"
    },
    {
      "line": 895,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 896,
      "value": "#Then Submit individual settings for_\u003cTestCaseID\u003e"
    },
    {
      "line": 897,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 898,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 899,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 900,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 901,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 902,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 903,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 904,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 905,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 906,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 907,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 908,
      "value": "#Then Add Employee in a Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 909,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 910,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 911,
      "value": "#Then Get vehicles and escorts details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 912,
      "value": "#Then Add vehicle and escort to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 913,
      "value": "#Then Validate added escort for_\u003cTestCaseID\u003e"
    },
    {
      "line": 914,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 915,
      "value": "#Then Able to delete all request for_\u003cTestCaseID\u003e"
    },
    {
      "line": 916,
      "value": "#Examples:"
    },
    {
      "line": 917,
      "value": "#|TestCaseID|"
    },
    {
      "line": 918,
      "value": "#|TC_001|"
    },
    {
      "line": 920,
      "value": "#Scenario Outline: Ability to verify complete routing functionality for particular shift time"
    },
    {
      "line": 921,
      "value": "#Given Service end point is active"
    },
    {
      "line": 922,
      "value": "#Then Do Login"
    },
    {
      "line": 923,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 924,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 925,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 926,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 927,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 928,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 929,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 930,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 931,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 932,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 933,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 934,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 935,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 936,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 937,
      "value": "#Examples:"
    },
    {
      "line": 938,
      "value": "#|TestCaseID|"
    },
    {
      "line": 939,
      "value": "#|TC_001|"
    },
    {
      "line": 941,
      "value": "#Scenario Outline: Ability to verify particular route is getting deleted before complete routing"
    },
    {
      "line": 942,
      "value": "#Given Service end point is active"
    },
    {
      "line": 943,
      "value": "#Then Do Login"
    },
    {
      "line": 944,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 945,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 946,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 947,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 948,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 949,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 950,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 951,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 952,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 953,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 954,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 955,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 956,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 957,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 958,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 959,
      "value": "#Examples:"
    },
    {
      "line": 960,
      "value": "#|TestCaseID|"
    },
    {
      "line": 961,
      "value": "#|TC_001|"
    },
    {
      "line": 963,
      "value": "#Scenario Outline: Ability to verify particular route is getting deleted after complete routing"
    },
    {
      "line": 964,
      "value": "#Given Service end point is active"
    },
    {
      "line": 965,
      "value": "#Then Do Login"
    },
    {
      "line": 966,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 967,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 968,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 969,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 970,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 971,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 972,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 973,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 974,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 975,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 976,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 977,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 978,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 979,
      "value": "#Then Delete Route negative for_\u003cTestCaseID\u003e"
    },
    {
      "line": 980,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 981,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 982,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 983,
      "value": "#Examples:"
    },
    {
      "line": 984,
      "value": "#|TestCaseID|"
    },
    {
      "line": 985,
      "value": "#|TC_001|"
    },
    {
      "line": 987,
      "value": "#Scenario Outline: Ability to verify bucket close before complete routing"
    },
    {
      "line": 988,
      "value": "#Given Service end point is active"
    },
    {
      "line": 989,
      "value": "#Then Do Login"
    },
    {
      "line": 990,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 991,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 992,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 993,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 994,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 995,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 996,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 997,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 998,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 999,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1000,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1001,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1002,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1003,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1004,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1005,
      "value": "#Then Close the route before complete routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1006,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1007,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1008,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1009,
      "value": "#Examples:"
    },
    {
      "line": 1010,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1011,
      "value": "#|TC_001|"
    },
    {
      "line": 1013,
      "value": "#Scenario Outline: Ability to verify bucket close after complete routing"
    },
    {
      "line": 1014,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1015,
      "value": "#Then Do Login"
    },
    {
      "line": 1016,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1017,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1018,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1019,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1020,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1021,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1022,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1023,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1024,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1025,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1026,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1027,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1028,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1029,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1030,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1031,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1032,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1033,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1034,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1035,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1036,
      "value": "#Examples:"
    },
    {
      "line": 1037,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1038,
      "value": "#|TC_001|"
    },
    {
      "line": 1040,
      "value": "#Scenario Outline: Ability to verify bucket close before driver allocation"
    },
    {
      "line": 1041,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1042,
      "value": "#Then Do Login"
    },
    {
      "line": 1043,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1044,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1045,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1046,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1047,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1048,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1049,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1050,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1051,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1052,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1053,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1054,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1055,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1056,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1057,
      "value": "#Then Close the route before driver allocation for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1058,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1059,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1060,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1061,
      "value": "#Examples:"
    },
    {
      "line": 1062,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1063,
      "value": "#|TC_001|"
    },
    {
      "line": 1065,
      "value": "#Scenario Outline: Ability to verify print all before complete routing"
    },
    {
      "line": 1066,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1067,
      "value": "#Then Do Login"
    },
    {
      "line": 1068,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1069,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1070,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1071,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1072,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1073,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1074,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1075,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1076,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1077,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1078,
      "value": "#Then Print all before complete routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1079,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1080,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1081,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1082,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1083,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1084,
      "value": "#Examples:"
    },
    {
      "line": 1085,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1086,
      "value": "#|TC_001|"
    },
    {
      "line": 1088,
      "value": "#Scenario Outline: Ability to verify print all after complete routing"
    },
    {
      "line": 1089,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1090,
      "value": "#Then Do Login"
    },
    {
      "line": 1091,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1092,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1093,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1094,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1095,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1096,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1097,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1098,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1099,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1100,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1101,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1102,
      "value": "#Then Print all after complete routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1103,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1104,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1105,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1106,
      "value": "#Examples:"
    },
    {
      "line": 1107,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1108,
      "value": "#|TC_001|"
    },
    {
      "line": 1110,
      "value": "#Scenario Outline: Ability to verify manual trip end before status update"
    },
    {
      "line": 1111,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1112,
      "value": "#Then Do Login"
    },
    {
      "line": 1113,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1114,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1115,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1116,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1117,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1118,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1119,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1120,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1121,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1122,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1123,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1124,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1125,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1126,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1127,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1128,
      "value": "#Then Manually start the trip for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1129,
      "value": "#Then Manually close the trip before status update for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1130,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1131,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1132,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1133,
      "value": "#Examples:"
    },
    {
      "line": 1134,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1135,
      "value": "#|TC_001|"
    },
    {
      "line": 1137,
      "value": "#Scenario Outline: Ability to verify close route after closing the trip"
    },
    {
      "line": 1138,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1139,
      "value": "#Then Do Login"
    },
    {
      "line": 1140,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1141,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1142,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1143,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1144,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1145,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1146,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1147,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1148,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1149,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1150,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1151,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1152,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1153,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1154,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1155,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1156,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1157,
      "value": "#Then Validate closed route after closing trip for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1158,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1159,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1160,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1161,
      "value": "#Examples:"
    },
    {
      "line": 1162,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1163,
      "value": "#|TC_001|"
    },
    {
      "line": 1165,
      "value": "#Scenario Outline: Ability to verify close route before closing the trip"
    },
    {
      "line": 1166,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1167,
      "value": "#Then Do Login"
    },
    {
      "line": 1168,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1169,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1170,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1171,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1172,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1173,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1174,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1175,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1176,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1177,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1178,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1179,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1180,
      "value": "#Then Validate closed route before closing trip for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1181,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1182,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1183,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1184,
      "value": "#Examples:"
    },
    {
      "line": 1185,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1186,
      "value": "#|TC_001|"
    },
    {
      "line": 1188,
      "value": "#Scenario Outline: Ability to verify close routes but not started option"
    },
    {
      "line": 1189,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1190,
      "value": "#Then Do Login"
    },
    {
      "line": 1191,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1192,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1193,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1194,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1195,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1196,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1197,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1198,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1199,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1200,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1201,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1202,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1203,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1204,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1205,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1206,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1207,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1208,
      "value": "#Then Validate closed route but not started for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1209,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1210,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1211,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1212,
      "value": "#Examples:"
    },
    {
      "line": 1213,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1214,
      "value": "#|TC_001|"
    },
    {
      "line": 1216,
      "value": "#Scenario Outline: Ability to verify started routes option"
    },
    {
      "line": 1217,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1218,
      "value": "#Then Do Login"
    },
    {
      "line": 1219,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1220,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1221,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1222,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1223,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1224,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1225,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1226,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1227,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1228,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1229,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1230,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1231,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1232,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1233,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1234,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1235,
      "value": "#Then Manually start the trip for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1236,
      "value": "#Then Validate started routes option for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1237,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1238,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1239,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1240,
      "value": "#Examples:"
    },
    {
      "line": 1241,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1242,
      "value": "#|TC_001|"
    },
    {
      "line": 1244,
      "value": "#Scenario Outline: Ability to verify open routes option before complete routing"
    },
    {
      "line": 1245,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1246,
      "value": "#Then Do Login"
    },
    {
      "line": 1247,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1248,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1249,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1250,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1251,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1252,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1253,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1254,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1255,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1256,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1257,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1258,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1259,
      "value": "#Then Validate open routes option for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1260,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1261,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1262,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1263,
      "value": "#Examples:"
    },
    {
      "line": 1264,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1265,
      "value": "#|TC_001|"
    },
    {
      "line": 1267,
      "value": "#Scenario Outline: Ability to verify open routes option after complete routing"
    },
    {
      "line": 1268,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1269,
      "value": "#Then Do Login"
    },
    {
      "line": 1270,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1271,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1272,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1273,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1274,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1275,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1276,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1277,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1278,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1279,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1280,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1281,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1282,
      "value": "#Then Validate open routes option for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1283,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1284,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1285,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1286,
      "value": "#Examples:"
    },
    {
      "line": 1287,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1288,
      "value": "#|TC_001|"
    },
    {
      "line": 1290,
      "value": "#Scenario Outline: Ability to verify back to back option"
    },
    {
      "line": 1291,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1292,
      "value": "#Then Do Login"
    },
    {
      "line": 1293,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1294,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1295,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1296,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1297,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1298,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1299,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1300,
      "value": "#Then Get Shift Id for Back2Back Request_\u003cTestCaseID\u003e"
    },
    {
      "line": 1301,
      "value": "#Then Create a new request for cab booking for Back2Back Request_\u003cTestCaseID\u003e"
    },
    {
      "line": 1302,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1303,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1304,
      "value": "#Then Get Routing Details for Back2back Request_\u003cTestCaseID\u003e"
    },
    {
      "line": 1305,
      "value": "#Then Allocate cab in a route for back2back Request_\u003cTestCaseID\u003e"
    },
    {
      "line": 1306,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1307,
      "value": "#Then Validate Cab Routing for back2back Routing_\u003cTestCaseID\u003e"
    },
    {
      "line": 1308,
      "value": "#Then Able to perform Manual Back To Back Search for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1309,
      "value": "#Then Able to create Manual Back To Back Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1310,
      "value": "#Then Validate back to back routes option for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1311,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1312,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1313,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1314,
      "value": "#Then Delete Route for Back2Back Request_\u003cTestCaseID\u003e"
    },
    {
      "line": 1315,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1316,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1317,
      "value": "#Examples:"
    },
    {
      "line": 1318,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1319,
      "value": "#|TC_001|"
    },
    {
      "line": 1321,
      "value": "#Scenario Outline: Ability to verify google sequence option"
    },
    {
      "line": 1322,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1323,
      "value": "#Then Do Login"
    },
    {
      "line": 1324,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1325,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1326,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1327,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1328,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1329,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1330,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1331,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1332,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1333,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1334,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1335,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1336,
      "value": "#Then Validate google sequence option for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1337,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1338,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1339,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1340,
      "value": "#Examples:"
    },
    {
      "line": 1341,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1342,
      "value": "#|TC_001|"
    },
    {
      "line": 1344,
      "value": "#Scenario Outline: Ability to verify vendor wise option"
    },
    {
      "line": 1345,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1346,
      "value": "#Then Do Login"
    },
    {
      "line": 1347,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1348,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1349,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1350,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1351,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1352,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1353,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1354,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1355,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1356,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1357,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1358,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1359,
      "value": "#Then Able to get allActive vendors for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1360,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1361,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1362,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1363,
      "value": "#Examples:"
    },
    {
      "line": 1364,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1365,
      "value": "#|TC_001|"
    },
    {
      "line": 1367,
      "value": "#Scenario Outline: Ability to verify delivered on driver device option before close route"
    },
    {
      "line": 1368,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1369,
      "value": "#Then Do Login"
    },
    {
      "line": 1370,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1371,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1372,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1373,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1374,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1375,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1376,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1377,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1378,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1379,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1380,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1381,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1382,
      "value": "#Then Able to get delivered driver before close route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1383,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1384,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1385,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1386,
      "value": "#Examples:"
    },
    {
      "line": 1387,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1388,
      "value": "#|TC_001|"
    },
    {
      "line": 1390,
      "value": "#Scenario Outline: Ability to verify delivered on driver device option after close route"
    },
    {
      "line": 1391,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1392,
      "value": "#Then Do Login"
    },
    {
      "line": 1393,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1394,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1395,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1396,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1397,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1398,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1399,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1400,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1401,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1402,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1403,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1404,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1405,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1406,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1407,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1408,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1409,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1410,
      "value": "#Then Manually start the trip for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1411,
      "value": "#Then Able to get delivered driver after close route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1412,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1413,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1414,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1415,
      "value": "#Examples:"
    },
    {
      "line": 1416,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1417,
      "value": "#|TC_001|"
    },
    {
      "line": 1419,
      "value": "#Scenario Outline: Ability to verify tracking copy option"
    },
    {
      "line": 1420,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1421,
      "value": "#Then Do Login"
    },
    {
      "line": 1422,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1423,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1424,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1425,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1426,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1427,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1428,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1429,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1430,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1431,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1432,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1433,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1434,
      "value": "#Then Able to perform TrackingCopy for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1435,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1436,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1437,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1438,
      "value": "#Examples:"
    },
    {
      "line": 1439,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1440,
      "value": "#|TC_001|"
    },
    {
      "line": 1442,
      "value": "#Scenario Outline: Ability to verify download display sheet option"
    },
    {
      "line": 1443,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1444,
      "value": "#Then Do Login"
    },
    {
      "line": 1445,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1446,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1447,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1448,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1449,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1450,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1451,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1452,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1453,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1454,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1455,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1456,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1457,
      "value": "#Then Able to download display sheet for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1458,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1459,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1460,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1461,
      "value": "#Examples:"
    },
    {
      "line": 1462,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1463,
      "value": "#|TC_001|"
    },
    {
      "line": 1465,
      "value": "#Scenario Outline: Ability to verify download vendor wise tracking sheet option"
    },
    {
      "line": 1466,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1467,
      "value": "#Then Do Login"
    },
    {
      "line": 1468,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1469,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1470,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1471,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1472,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1473,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1474,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1475,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1476,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1477,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1478,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1479,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1480,
      "value": "#Then Able to download vendor wise tracking sheet for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1481,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1482,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1483,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1484,
      "value": "#Examples:"
    },
    {
      "line": 1485,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1486,
      "value": "#|TC_001|"
    },
    {
      "line": 1488,
      "value": "#Scenario Outline: Ability to verify download routing sheet option"
    },
    {
      "line": 1489,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1490,
      "value": "#Then Do Login"
    },
    {
      "line": 1491,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1492,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1493,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1494,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1495,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1496,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1497,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1498,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1499,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1500,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1501,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1502,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1503,
      "value": "#Then Able to download routing sheet for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1504,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1505,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1506,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1507,
      "value": "#Examples:"
    },
    {
      "line": 1508,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1509,
      "value": "#|TC_001|"
    },
    {
      "line": 1511,
      "value": "#Scenario Outline: Ability to verify download trip summary sheet option"
    },
    {
      "line": 1512,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1513,
      "value": "#Then Do Login"
    },
    {
      "line": 1514,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1515,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1516,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1517,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1518,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1519,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1520,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1521,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1522,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1523,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1524,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1525,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1526,
      "value": "#Then Able to download trip summary sheet for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1527,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1528,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1529,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1530,
      "value": "#Examples:"
    },
    {
      "line": 1531,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1532,
      "value": "#|TC_001|"
    },
    {
      "line": 1534,
      "value": "#Scenario Outline: Ability to get shuttle allocated trip details"
    },
    {
      "line": 1535,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1536,
      "value": "#Then Do Login"
    },
    {
      "line": 1537,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1538,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1539,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1540,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1541,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1542,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1543,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1544,
      "value": "#Then Getting nodalroutes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1545,
      "value": "#Then Getting Request count for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1546,
      "value": "#Then createNormal Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1547,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1548,
      "value": "#Then Able to get shuttle allocated trips for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1549,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1550,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1551,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1552,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1553,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1554,
      "value": "#Examples:"
    },
    {
      "line": 1555,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1556,
      "value": "#|TC_001|"
    },
    {
      "line": 1558,
      "value": "#Scenario Outline: Ability to verify delete all shift created by mistake before complete routing"
    },
    {
      "line": 1559,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1560,
      "value": "#Then Do Login"
    },
    {
      "line": 1561,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1562,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1563,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1564,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1565,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1566,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1567,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1568,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1569,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1570,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1571,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1572,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1573,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1574,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1575,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1576,
      "value": "#Examples:"
    },
    {
      "line": 1577,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1578,
      "value": "#|TC_001|"
    },
    {
      "line": 1580,
      "value": "#Scenario Outline: Ability to get allocated but not closed routed details"
    },
    {
      "line": 1581,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1582,
      "value": "#Then Do Login"
    },
    {
      "line": 1583,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1584,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1585,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1586,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1587,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1588,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1589,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1590,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1591,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1592,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1593,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1594,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1595,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1596,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1597,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1598,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1599,
      "value": "#Then Able to get allocated but not closed routed details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1600,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1601,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1602,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1603,
      "value": "#Examples:"
    },
    {
      "line": 1604,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1605,
      "value": "#|TC_001|"
    },
    {
      "line": 1607,
      "value": "#Scenario Outline: Ability to get escort required trip"
    },
    {
      "line": 1608,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1609,
      "value": "#Then Do Login"
    },
    {
      "line": 1610,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1611,
      "value": "#Then Submit individual settings for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1612,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1613,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1614,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1615,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1616,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1617,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1618,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1619,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1620,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1621,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1622,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1623,
      "value": "#Then Add Employee in a Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1624,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1625,
      "value": "#Then Able to get escort required trips for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1626,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1627,
      "value": "#Then Able to delete all request for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1628,
      "value": "#Examples:"
    },
    {
      "line": 1629,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1630,
      "value": "#|TC_001|"
    },
    {
      "line": 1632,
      "value": "#Scenario Outline: Ability to get special route details"
    },
    {
      "line": 1633,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1634,
      "value": "#Then Do Login"
    },
    {
      "line": 1635,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1636,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1637,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1638,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1639,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1640,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1641,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1642,
      "value": "#Then Getting nodalroutes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1643,
      "value": "#Then Getting Request count for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1644,
      "value": "#Then Create Special Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1645,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1646,
      "value": "#Then Able to get special routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1647,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1648,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1649,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1650,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1651,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1652,
      "value": "#Examples:"
    },
    {
      "line": 1653,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1654,
      "value": "#|TC_001|"
    },
    {
      "line": 1656,
      "value": "#Scenario Outline: Ability to get routing dump on email"
    },
    {
      "line": 1657,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1658,
      "value": "#Then Do Login"
    },
    {
      "line": 1659,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1660,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1661,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1662,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1663,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1664,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1665,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1666,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1667,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1668,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1669,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1670,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1671,
      "value": "#Then Able to get routing dump on email for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1672,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1673,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1674,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1675,
      "value": "#Examples:"
    },
    {
      "line": 1676,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1677,
      "value": "#|TC_001|"
    },
    {
      "line": 1679,
      "value": "#Scenario Outline: Ability to verify swapping employee from one route to another route"
    },
    {
      "line": 1680,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1681,
      "value": "#Then Do Login"
    },
    {
      "line": 1682,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1683,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1684,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1685,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1686,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1687,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1688,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1689,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1690,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1691,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1692,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1693,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1694,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1695,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1696,
      "value": "#Then Create new route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1697,
      "value": "#Then Swap employee from one route to another route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1698,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1699,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1700,
      "value": "#Examples:"
    },
    {
      "line": 1701,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1702,
      "value": "#|TC_001|"
    },
    {
      "line": 1704,
      "value": "#Scenario Outline: Ability to verify swapping employee from one route to another route after complete routing"
    },
    {
      "line": 1705,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1706,
      "value": "#Then Do Login"
    },
    {
      "line": 1707,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1708,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1709,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1710,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1711,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1712,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1713,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1714,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1715,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1716,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1717,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1718,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1719,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1720,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1721,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1722,
      "value": "#Then Create new route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1723,
      "value": "#Then Swap employee from one route to another route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1724,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1725,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1726,
      "value": "#Examples:"
    },
    {
      "line": 1727,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1728,
      "value": "#|TC_001|"
    },
    {
      "line": 1730,
      "value": "#Scenario Outline: Ability to verify start trip before complete routing"
    },
    {
      "line": 1731,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1732,
      "value": "#Then Do Login"
    },
    {
      "line": 1733,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1734,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1735,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1736,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1737,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1738,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1739,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1740,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1741,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1742,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1743,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1744,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1745,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1746,
      "value": "#Then Manually start the trip with dummy entry for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1747,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1748,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1749,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1750,
      "value": "#Examples:"
    },
    {
      "line": 1751,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1752,
      "value": "#|TC_001|"
    },
    {
      "line": 1754,
      "value": "#Scenario Outline: Ability to verify manual trip end before status update"
    },
    {
      "line": 1755,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1756,
      "value": "#Then Do Login"
    },
    {
      "line": 1757,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1758,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1759,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1760,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1761,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1762,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1763,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1764,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1765,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1766,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1767,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1768,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1769,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1770,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1771,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1772,
      "value": "#Then Manually start the trip for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1773,
      "value": "#Then Manually close the trip before status update for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1774,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1775,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1776,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1777,
      "value": "#Examples:"
    },
    {
      "line": 1778,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1779,
      "value": "#|TC_001|"
    },
    {
      "line": 1781,
      "value": "#Scenario Outline: Ability to view closed routes"
    },
    {
      "line": 1782,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1783,
      "value": "#Then Do Login"
    },
    {
      "line": 1784,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1785,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1786,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1787,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1788,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1789,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1790,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1791,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1792,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1793,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1794,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1795,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1796,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1797,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1798,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1799,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1800,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1801,
      "value": "#Then Able to view closed routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1802,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1803,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1804,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1805,
      "value": "#Examples:"
    },
    {
      "line": 1806,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1807,
      "value": "#|TC_001|"
    },
    {
      "line": 1809,
      "value": "#Scenario Outline: Ability to start all routes"
    },
    {
      "line": 1810,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1811,
      "value": "#Then Do Login"
    },
    {
      "line": 1812,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1813,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1814,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1815,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1816,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1817,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1818,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1819,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1820,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1821,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1822,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1823,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1824,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1825,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1826,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1827,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1828,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1829,
      "value": "#Then Able to view closed routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1830,
      "value": "#Then Able to start all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1831,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1832,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1833,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1834,
      "value": "#Examples:"
    },
    {
      "line": 1835,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1836,
      "value": "#|TC_001|"
    },
    {
      "line": 1838,
      "value": "#Scenario Outline: Ability to verify started trips are showing are not"
    },
    {
      "line": 1839,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1840,
      "value": "#Then Do Login"
    },
    {
      "line": 1841,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1842,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1843,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1844,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1845,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1846,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1847,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1848,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1849,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1850,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1851,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1852,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1853,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1854,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1855,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1856,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1857,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1858,
      "value": "#Then Able to view closed routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1859,
      "value": "#Then Able to start all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1860,
      "value": "#Then Able to verify started routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1861,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1862,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1863,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1864,
      "value": "#Examples:"
    },
    {
      "line": 1865,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1866,
      "value": "#|TC_001|"
    },
    {
      "line": 1868,
      "value": "#Scenario Outline: Ability to view all started routes"
    },
    {
      "line": 1869,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1870,
      "value": "#Then Do Login"
    },
    {
      "line": 1871,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1872,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1873,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1874,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1875,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1876,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1877,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1878,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1879,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1880,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1881,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1882,
      "value": "#Then Able to Complete Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1883,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1884,
      "value": "#Then Get route id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1885,
      "value": "#Then Get vehicles details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1886,
      "value": "#Then Add vehicle to route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1887,
      "value": "#Then Close the route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1888,
      "value": "#Then Able to view closed routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1889,
      "value": "#Then Able to start all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1890,
      "value": "#Then Able to view started trips for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1891,
      "value": "#Then Delete all routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1892,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1893,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1894,
      "value": "#Examples:"
    },
    {
      "line": 1895,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1896,
      "value": "#|TC_001|"
    },
    {
      "line": 1898,
      "value": "#Scenario Outline: Ability to verify allocate vendor to route button"
    },
    {
      "line": 1899,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1900,
      "value": "#Then Do Login"
    },
    {
      "line": 1901,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1902,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1903,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1904,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1905,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1906,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1907,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1908,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1909,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1910,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1911,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1912,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1913,
      "value": "#Then Verify allocate vendor to route button for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1914,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1915,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1916,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1917,
      "value": "#Examples:"
    },
    {
      "line": 1918,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1919,
      "value": "#|TC_001|"
    },
    {
      "line": 1921,
      "value": "#Scenario Outline: Ability to view vendor details based on facility"
    },
    {
      "line": 1922,
      "value": "#Given Service end point is active"
    },
    {
      "line": 1923,
      "value": "#Then Do Login"
    },
    {
      "line": 1924,
      "value": "#Then Submit request settings_businessdays for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1925,
      "value": "#Then Get Project Name Zone Name for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1926,
      "value": "#Then Get Process Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1927,
      "value": "#Then Get Zone Id for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1928,
      "value": "#Then Create shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1929,
      "value": "#Then Validate shift Time for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1930,
      "value": "#Then Create a new request for cab booking for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1931,
      "value": "#Then Get Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1932,
      "value": "#Then Allocate cab in a route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1933,
      "value": "#Then Validate Cab Routing for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1934,
      "value": "#Then Able to get All_Routes for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1935,
      "value": "#Then View Routing Details for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1936,
      "value": "#Then Able to get allActive vendors for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1937,
      "value": "#Then Delete Route for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1938,
      "value": "#Then View booking schedule for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1939,
      "value": "#Then Cancelcab for_\u003cTestCaseID\u003e"
    },
    {
      "line": 1940,
      "value": "#Examples:"
    },
    {
      "line": 1941,
      "value": "#|TestCaseID|"
    },
    {
      "line": 1942,
      "value": "#|TC_001|"
    }
  ],
  "line": 1944,
  "name": "Ability to verify selected route allocated vendor",
  "description": "",
  "id": "routing-cab-allocation;ability-to-verify-selected-route-allocated-vendor",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 1945,
  "name": "Service end point is active",
  "keyword": "Given "
});
formatter.step({
  "line": 1946,
  "name": "Do Login",
  "keyword": "Then "
});
formatter.step({
  "line": 1947,
  "name": "Submit request settings_businessdays for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1948,
  "name": "Get Project Name Zone Name for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1949,
  "name": "Get Process Id for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1950,
  "name": "Get Zone Id for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1951,
  "name": "Create shift Time for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1952,
  "name": "Validate shift Time for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1953,
  "name": "Create a new request for cab booking for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1954,
  "name": "Get Routing Details for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1955,
  "name": "Allocate cab in a route for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1956,
  "name": "Validate Cab Routing for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1957,
  "name": "Able to get All_Routes for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1958,
  "name": "View Routing Details for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1959,
  "name": "Allocate Vendor in a route for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1960,
  "name": "Delete Route for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1961,
  "name": "View booking schedule for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.step({
  "line": 1962,
  "name": "Cancelcab for_\u003cTestCaseID\u003e",
  "keyword": "Then "
});
formatter.examples({
  "line": 1963,
  "name": "",
  "description": "",
  "id": "routing-cab-allocation;ability-to-verify-selected-route-allocated-vendor;",
  "rows": [
    {
      "cells": [
        "TestCaseID"
      ],
      "line": 1964,
      "id": "routing-cab-allocation;ability-to-verify-selected-route-allocated-vendor;;1"
    },
    {
      "cells": [
        "TC_001"
      ],
      "line": 1965,
      "id": "routing-cab-allocation;ability-to-verify-selected-route-allocated-vendor;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 1965,
  "name": "Ability to verify selected route allocated vendor",
  "description": "",
  "id": "routing-cab-allocation;ability-to-verify-selected-route-allocated-vendor;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 1945,
  "name": "Service end point is active",
  "keyword": "Given "
});
formatter.step({
  "line": 1946,
  "name": "Do Login",
  "keyword": "Then "
});
formatter.step({
  "line": 1947,
  "name": "Submit request settings_businessdays for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1948,
  "name": "Get Project Name Zone Name for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1949,
  "name": "Get Process Id for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1950,
  "name": "Get Zone Id for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1951,
  "name": "Create shift Time for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1952,
  "name": "Validate shift Time for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1953,
  "name": "Create a new request for cab booking for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1954,
  "name": "Get Routing Details for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1955,
  "name": "Allocate cab in a route for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1956,
  "name": "Validate Cab Routing for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1957,
  "name": "Able to get All_Routes for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1958,
  "name": "View Routing Details for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1959,
  "name": "Allocate Vendor in a route for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1960,
  "name": "Delete Route for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1961,
  "name": "View booking schedule for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 1962,
  "name": "Cancelcab for_TC_001",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinitions.applicationToBeLive()"
});
formatter.result({
  "duration": 3211925300,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitions.doLogin()"
});
formatter.result({
  "duration": 1927797500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 41
    }
  ],
  "location": "StepDefinitions.requestSettings(String)"
});
formatter.result({
  "duration": 1943355300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 31
    }
  ],
  "location": "StepDefinitions.getProjectNameAndZoneName(String)"
});
formatter.result({
  "duration": 1935313100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 19
    }
  ],
  "location": "StepDefinitions.getProcessId(String)"
});
formatter.result({
  "duration": 507719200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 16
    }
  ],
  "location": "StepDefinitions.getZoneId(String)"
});
formatter.result({
  "duration": 381273700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 22
    }
  ],
  "location": "StepDefinitions.createShiftTime(String)"
});
formatter.result({
  "duration": 416072200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 24
    }
  ],
  "location": "StepDefinitions.validateShiftTime(String)"
});
formatter.result({
  "duration": 377395200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 41
    }
  ],
  "location": "StepDefinitions.BookACab(String)"
});
formatter.result({
  "duration": 1210558700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 24
    }
  ],
  "location": "StepDefinitions.getRoutingDetails(String)"
});
formatter.result({
  "duration": 617434000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 28
    }
  ],
  "location": "StepDefinitions.CabRouting(String)"
});
formatter.result({
  "duration": 1063263800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 25
    }
  ],
  "location": "StepDefinitions.validateCabRouting(String)"
});
formatter.result({
  "duration": 580222900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 27
    }
  ],
  "location": "StepDefinitions.All_Routes(String)"
});
formatter.result({
  "duration": 621953600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 25
    }
  ],
  "location": "StepDefinitions.viewRoutingDetails(String)"
});
formatter.result({
  "duration": 387957800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 31
    }
  ],
  "location": "StepDefinitions.allocateVendor(String)"
});
formatter.result({
  "duration": 307775100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 17
    }
  ],
  "location": "StepDefinitions.deleteRoute(String)"
});
formatter.result({
  "duration": 630733400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 26
    }
  ],
  "location": "StepDefinitions.ViewBookingSchedule(String)"
});
formatter.result({
  "duration": 566481700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC_001",
      "offset": 14
    }
  ],
  "location": "StepDefinitions.cancelcab(String)"
});
formatter.result({
  "duration": 474625300,
  "status": "passed"
});
});