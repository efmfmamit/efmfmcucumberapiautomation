Feature: Request Details
    Scenario Outline: Cab Request by Employee before Cut Off Time based on Business days
    Given Service end point is active
    Then Do Login
    Then Submit request settings_businessdays for_<TestCaseID>
    Then Get Project Name Zone Name for_<TestCaseID>
    Then Get Process Id for_<TestCaseID>
    Then Get Zone Id for_<TestCaseID>
    Then Create shift Time for_<TestCaseID>
    Then Validate shift Time for_<TestCaseID>
    Then Create a new request for cab booking for_<TestCaseID>
    Then View booking schedule for_<TestCaseID>
    Then Get Shift Id for_<TestCaseID>
    Then Rescheule cab for_<TestCaseID>
    Then Cancelcab for_<TestCaseID>
    Examples:
    |TestCaseID|
    |TC_001|
    
    Scenario Outline: Cab Request by Employee before Cut Off Time based on trip type
    Given Service end point is active
    Then Do Login
    Then Submit request settings_triptype for_<TestCaseID>
    Then Get Project Name Zone Name for_<TestCaseID>
    Then Get Process Id for_<TestCaseID>
    Then Get Zone Id for_<TestCaseID>
    Then Create shift Time for_<TestCaseID>
    Then Validate shift Time for_<TestCaseID>
    Then Create a new request for cab booking for_<TestCaseID>
    Then View booking schedule for_<TestCaseID>
    Then Get Shift Id for_<TestCaseID>
    Then Rescheule cab for_<TestCaseID>
    Then Cancelcab for_<TestCaseID>
    Examples:
    |TestCaseID|
    |TC_001|
    
    Scenario Outline: Manager book cab for employee before cut off time based on Business days
    Given Service end point is active
    Then Do Login Manager
    Then Submit request settings_businessdays for_<TestCaseID>
    Then Get Project Name Zone Name for_<TestCaseID>
    Then Get Process Id for_<TestCaseID>
    Then Get Zone Id for_<TestCaseID>
    Then Create shift Time for_<TestCaseID>
    Then Get project Id manager for_<TestCaseID>
    Then Get Manager Booking Shift Id for_<TestCaseID>
    Then Create a new request for cab booking manager for_<TestCaseID>
    Then View DR booking schedule for_<TestCaseID>
    Then Get Manager Reschdule Shift Id for_<TestCaseID>
    Then Reschedule cab Request Manger for_<TestCaseID>
    Then CancelcabpositiveManager for_<TestCaseID>
    Examples:
    |TestCaseID|
    |TC_001|
    
    Scenario Outline: Manager book cab for employee before cut off time based on trip type
    Given Service end point is active
    Then Do Login Manager
    Then Submit request settings_triptype for_<TestCaseID>
    Then Get Project Name Zone Name for_<TestCaseID>
    Then Get Process Id for_<TestCaseID>
    Then Get Zone Id for_<TestCaseID>
    Then Create shift Time for_<TestCaseID>
    Then Get project Id manager for_<TestCaseID>
    Then Get Manager Booking Shift Id for_<TestCaseID>
    Then Create a new request for cab booking manager for_<TestCaseID>
    Then View DR booking schedule for_<TestCaseID>
    Then Get Manager Reschdule Shift Id for_<TestCaseID>
    Then Reschedule cab Request Manger for_<TestCaseID>
    Then CancelcabpositiveManager for_<TestCaseID>
    Examples:
    |TestCaseID|
    |TC_001|
    
    #Scenario Outline: Ability to Book a cab as a SPOC based on Business days
    #Given Service end point is active
    #Then Do Login Spoc
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab as spoc for_<TestCaseID>
    #Then View SPOC booking schedule for_<TestCaseID>
    #Then Get SPOC Reschdule Shift Id for_<TestCaseID>
    #Then Reschedule cab  positive as SPOC for_<TestCaseID>
    #Then CancelcabSPOC for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #Scenario Outline: Ability to Book a cab as a SPOC based on Trip Type
    #Given Service end point is active
    #Then Do Login Spoc
    #Then Create shift Time for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab as spoc for_<TestCaseID>
    #Then View SPOC booking schedule for_<TestCaseID>
    #Then Get SPOC Reschdule Shift Id for_<TestCaseID>
    #Then Reschedule cab  positive as SPOC for_<TestCaseID>
    #Then CancelcabSPOC for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
  #
    Scenario Outline: Create a a cab after cut off time as a employee based on Trip type
    Given Service end point is active
    Then Do Login
    Then Submit request settings_triptype_negative for_<TestCaseID>
    Then Get Project Name Zone Name for_<TestCaseID>
    Then Get Process Id for_<TestCaseID>
    Then Get Zone Id for_<TestCaseID>
    Then Create shift Time for_<TestCaseID>
    Then Validate shift Time for_<TestCaseID>
    Then Create a new request for cab booking negative scenario for_<TestCaseID>
    Then Submit request settings_triptype for_<TestCaseID>
    Then Get Project Name Zone Name for_<TestCaseID>
    Then Get Process Id for_<TestCaseID>
    Then Get Zone Id for_<TestCaseID>
    Then Create shift Time for_<TestCaseID>
    Then Validate shift Time for_<TestCaseID>
    Then Create a new request for cab booking for_<TestCaseID>
    Then View booking schedule for_<TestCaseID>
    Then Submit request settings_triptype_negative for_<TestCaseID>
    Then Get Shift Id for_<TestCaseID>
    Then Rescheule cab_negative for_<TestCaseID>
    Then Cancelcabnegative for_<TestCaseID>
    Then Submit request settings_triptype for_<TestCaseID>
    Then Cancelcab for_<TestCaseID>
    Examples:
    |TestCaseID|
    |TC_001|
    
    #
    Scenario Outline: Create a a cab after cut off time as a employee based on Business days
    Given Service end point is active
    Then Do Login
    Then Submit request settings_businessdays_negative for_<TestCaseID>
    Then Get Project Name Zone Name for_<TestCaseID>
    Then Get Process Id for_<TestCaseID>
    Then Get Zone Id for_<TestCaseID>
    Then Create shift Time for_<TestCaseID>
    Then Validate shift Time for_<TestCaseID>
    Then Create a new request for cab booking negative scenario for_<TestCaseID>
    Then Submit request settings_businessdays for_<TestCaseID>
    Then Get Project Name Zone Name for_<TestCaseID>
    Then Get Process Id for_<TestCaseID>
    Then Get Zone Id for_<TestCaseID>
    Then Create shift Time for_<TestCaseID>
    Then Validate shift Time for_<TestCaseID>
    Then Create a new request for cab booking for_<TestCaseID>
    Then View booking schedule for_<TestCaseID>
    Then Submit request settings_businessdays_negative for_<TestCaseID>
    Then Get Shift Id for_<TestCaseID>
    Then Rescheule cab_negative for_<TestCaseID>
    Then Cancelcabnegative for_<TestCaseID>
    Then Submit request settings_businessdays for_<TestCaseID>
    Then Cancelcab for_<TestCaseID>
    Examples:
    |TestCaseID|
    |TC_001|
    
    Scenario Outline: Manager book cab for employee after cut off time based on Business days
    Given Service end point is active
    Then Do Login Manager
    Then Submit request settings_businessdays_negative for_<TestCaseID>
    Then Get Project Name Zone Name for_<TestCaseID>
    Then Get Process Id for_<TestCaseID>
    Then Get Zone Id for_<TestCaseID>
    Then Create shift Time for_<TestCaseID>
    Then Get project Id manager for_<TestCaseID>
    Then Get Manager Booking Shift Id for_<TestCaseID>
    Then Create a new request for cab booking manager negative for_<TestCaseID>
    Then Submit request settings_businessdays for_<TestCaseID>
    Then Get project Id manager for_<TestCaseID>
    Then Get Manager Booking Shift Id for_<TestCaseID>
    Then Create a new request for cab booking manager for_<TestCaseID>
    Then View DR booking schedule for_<TestCaseID>
    Then Submit request settings_businessdays_negative for_<TestCaseID>
    Then Get Manager Reschdule Shift Id for_<TestCaseID>
    Then Reschdule cab Request Manger Negative for_<TestCaseID>
    Then CancelcabnegativeManager for_<TestCaseID>
    Then Submit request settings_businessdays for_<TestCaseID>
    Then CancelcabpositiveManager for_<TestCaseID>
    Examples:
    |TestCaseID|
    |TC_001|
    
    #Scenario Outline: Manager book cab for employee after cut off time based on Trip type
    #Given Service end point is active
    #Then Do Login Manager
    #Then Submit request settings_triptype_negative for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then Get project Id manager for_<TestCaseID>
    #Then Get Manager Booking Shift Id for_<TestCaseID>
    #Then Create a new request for cab booking manager negative for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then Get project Id manager for_<TestCaseID>
    #Then Get Manager Booking Shift Id for_<TestCaseID>
    #Then Create a new request for cab booking manager for_<TestCaseID>
    #Then View DR booking schedule for_<TestCaseID>
    #Then Submit request settings_triptype_negative for_<TestCaseID>
    #Then Get Manager Reschdule Shift Id for_<TestCaseID>
    #Then Reschdule cab Request Manger Negative for_<TestCaseID>
    #Then CancelcabnegativeManager for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then CancelcabpositiveManager for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #Scenario Outline: SPOC book cab for employee after cut off time based on Trip type
    #Given Service end point is active
    #Then Do Login Spoc
    #Then Submit request settings_triptype_negative for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab negative as spoc for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab as spoc for_<TestCaseID>
    #Then View SPOC booking schedule for_<TestCaseID>
    #Then Submit request settings_triptype_negative for_<TestCaseID>
    #Then Get SPOC Reschdule Shift Id for_<TestCaseID>
    #Then Reschedule cab  negative as SPOC for_<TestCaseID>
    #Then CancelcabnegativeSPOC for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then CancelcabSPOC for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #Scenario Outline: SPOC book cab for employee after cut off time based on Business days
    #Given Service end point is active
    #Then Do Login Spoc
    #Then Submit request settings_businessdays_negative for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
#		Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab negative as spoc for_<TestCaseID>
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab as spoc for_<TestCaseID>
    #Then View SPOC booking schedule for_<TestCaseID>
    #Then Submit request settings_businessdays_negative for_<TestCaseID>
    #Then Get SPOC Reschdule Shift Id for_<TestCaseID>
    #Then Reschedule cab  negative as SPOC for_<TestCaseID>
    #Then CancelcabnegativeSPOC for_<TestCaseID>
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then CancelcabSPOC for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #*******************************************************************************************
     #Scenario Outline: Cab Request by Employee before Cut Off Time based on Business days on Weekends
    #Given Service end point is active
    #Then Do Login
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for Weekend for_<TestCaseID>
    #Then Validate shift Time for Weekend for_<TestCaseID>
    #Then Create a new request for cab booking for Weekend for_<TestCaseID>
    #Then View booking schedule for_<TestCaseID>
    #Then Get Shift Id Weekend for_<TestCaseID>
    #Then Rescheule cab for_<TestCaseID>
    #Then Cancelcab for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
   #**************************************************************************************************** 
    #Scenario Outline: Cab Request by Employee before Cut Off Time based on trip type on Weekends
    #Given Service end point is active
    #Then Do Login
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then Validate shift Time for_<TestCaseID>
    #Then Create a new request for cab booking for_<TestCaseID>
    #Then View booking schedule for_<TestCaseID>
    #Then Get Shift Id for_<TestCaseID>
    #Then Rescheule cab for_<TestCaseID>
    #Then Cancelcab for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    
    #Scenario Outline: Manager book cab for employee before cut off time based on Business days on Weekends
    #Given Service end point is active
    #Then Do Login Manager
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then Get project Id manager for_<TestCaseID>
    #Then Get Manager Booking Shift Id for_<TestCaseID>
    #Then Create a new request for cab booking manager for_<TestCaseID>
    #Then View DR booking schedule for_<TestCaseID>
    #Then Get Manager Reschdule Shift Id for_<TestCaseID>
    #Then Reschedule cab Request Manger for_<TestCaseID>
    #Then CancelcabpositiveManager for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #Scenario Outline: Manager book cab for employee before cut off time based on trip type on Weekends
    #Given Service end point is active
    #Then Do Login Manager
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then Get project Id manager for_<TestCaseID>
    #Then Get Manager Booking Shift Id for_<TestCaseID>
    #Then Create a new request for cab booking manager for_<TestCaseID>
    #Then View DR booking schedule for_<TestCaseID>
    #Then Get Manager Reschdule Shift Id for_<TestCaseID>
    #Then Reschedule cab Request Manger for_<TestCaseID>
    #Then CancelcabpositiveManager for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #Scenario Outline: Ability to Book a cab as a SPOC based on Business days on Weekends 
    #Given Service end point is active
    #Then Do Login Spoc
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab as spoc for_<TestCaseID>
    #Then View SPOC booking schedule for_<TestCaseID>
    #Then Get SPOC Reschdule Shift Id for_<TestCaseID>
    #Then Reschedule cab  positive as SPOC for_<TestCaseID>
    #Then CancelcabSPOC for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #Scenario Outline: Ability to Book a cab as a SPOC based on Trip Type on Weekends
    #Given Service end point is active
    #Then Do Login Spoc
    #Then Create shift Time for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab as spoc for_<TestCaseID>
    #Then View SPOC booking schedule for_<TestCaseID>
    #Then Get SPOC Reschdule Shift Id for_<TestCaseID>
    #Then Reschedule cab  positive as SPOC for_<TestCaseID>
    #Then CancelcabSPOC for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
  #
    #Scenario Outline: Create a a cab after cut off time as a employee based on Trip type on Weekends
    #Given Service end point is active
    #Then Do Login
    #Then Submit request settings_triptype_negative for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then Validate shift Time for_<TestCaseID>
    #Then Create a new request for cab booking negative scenario for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then Validate shift Time for_<TestCaseID>
    #Then Create a new request for cab booking for_<TestCaseID>
    #Then View booking schedule for_<TestCaseID>
    #Then Submit request settings_triptype_negative for_<TestCaseID>
    #Then Get Shift Id for_<TestCaseID>
    #Then Rescheule cab_negative for_<TestCaseID>
    #Then Cancelcabnegative for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then Cancelcab for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #
    #Scenario Outline: Create a a cab after cut off time as a employee based on Business days 
    #Given Service end point is active
    #Then Do Login
    #Then Submit request settings_businessdays_negative for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then Validate shift Time for_<TestCaseID>
    #Then Create a new request for cab booking negative scenario for_<TestCaseID>
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then Validate shift Time for_<TestCaseID>
    #Then Create a new request for cab booking for_<TestCaseID>
    #Then View booking schedule for_<TestCaseID>
    #Then Submit request settings_businessdays_negative for_<TestCaseID>
    #Then Get Shift Id for_<TestCaseID>
    #Then Rescheule cab_negative for_<TestCaseID>
    #Then Cancelcabnegative for_<TestCaseID>
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then Cancelcab for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #Scenario Outline: Manager book cab for employee after cut off time based on Business days
    #Given Service end point is active
    #Then Do Login Manager
    #Then Submit request settings_businessdays_negative for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then Get project Id manager for_<TestCaseID>
    #Then Get Manager Booking Shift Id for_<TestCaseID>
    #Then Create a new request for cab booking manager negative for_<TestCaseID>
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then Get project Id manager for_<TestCaseID>
    #Then Get Manager Booking Shift Id for_<TestCaseID>
    #Then Create a new request for cab booking manager for_<TestCaseID>
    #Then View DR booking schedule for_<TestCaseID>
    #Then Submit request settings_businessdays_negative for_<TestCaseID>
    #Then Get Manager Reschdule Shift Id for_<TestCaseID>
    #Then Reschdule cab Request Manger Negative for_<TestCaseID>
    #Then CancelcabnegativeManager for_<TestCaseID>
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then CancelcabpositiveManager for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #Scenario Outline: Manager book cab for employee after cut off time based on Trip type
    #Given Service end point is active
    #Then Do Login Manager
    #Then Submit request settings_triptype_negative for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then Get project Id manager for_<TestCaseID>
    #Then Get Manager Booking Shift Id for_<TestCaseID>
    #Then Create a new request for cab booking manager negative for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then Get project Id manager for_<TestCaseID>
    #Then Get Manager Booking Shift Id for_<TestCaseID>
    #Then Create a new request for cab booking manager for_<TestCaseID>
    #Then View DR booking schedule for_<TestCaseID>
    #Then Submit request settings_triptype_negative for_<TestCaseID>
    #Then Get Manager Reschdule Shift Id for_<TestCaseID>
    #Then Reschdule cab Request Manger Negative for_<TestCaseID>
    #Then CancelcabnegativeManager for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then CancelcabpositiveManager for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #Scenario Outline: SPOC book cab for employee after cut off time based on Trip type
    #Given Service end point is active
    #Then Do Login Spoc
    #Then Submit request settings_triptype_negative for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
    #Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab negative as spoc for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab as spoc for_<TestCaseID>
    #Then View SPOC booking schedule for_<TestCaseID>
    #Then Submit request settings_triptype_negative for_<TestCaseID>
    #Then Get SPOC Reschdule Shift Id for_<TestCaseID>
    #Then Reschedule cab  negative as SPOC for_<TestCaseID>
    #Then CancelcabnegativeSPOC for_<TestCaseID>
    #Then Submit request settings_triptype for_<TestCaseID>
    #Then CancelcabSPOC for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    #
    #Scenario Outline: SPOC book cab for employee after cut off time based on Business days
    #Given Service end point is active
    #Then Do Login Spoc
    #Then Submit request settings_businessdays_negative for_<TestCaseID>
    #Then Get Project Name Zone Name for_<TestCaseID>
    #Then Get Process Id for_<TestCaseID>
    #Then Get Zone Id for_<TestCaseID>
    #Then Create shift Time for_<TestCaseID>
#		Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab negative as spoc for_<TestCaseID>
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then GetShiftTime_SPOC for_<TestCaseID>
    #Then Book a cab as spoc for_<TestCaseID>
    #Then View SPOC booking schedule for_<TestCaseID>
    #Then Submit request settings_businessdays_negative for_<TestCaseID>
    #Then Get SPOC Reschdule Shift Id for_<TestCaseID>
    #Then Reschedule cab  negative as SPOC for_<TestCaseID>
    #Then CancelcabnegativeSPOC for_<TestCaseID>
    #Then Submit request settings_businessdays for_<TestCaseID>
    #Then CancelcabSPOC for_<TestCaseID>
    #Examples:
    #|TestCaseID|
    #|TC_001|
    