{
  "userId": "$$userId$$",
  "combinedFacility": "$$facility$$",
  "eFmFmEmployeeRequestMaster": {
    "efmFmUserMaster": {
      "eFmFmClientBranchPO": {
        "branchId": "$$facility$$"
      }
    }
  },
  "efmFmUserMaster": {
    "userId": "$$userId$$"
  },
  "searchType": "Self",
  "startPgNo": 0,
  "endPgNo": "10"
}