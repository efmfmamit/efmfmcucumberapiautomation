{
  "userId": "$$userId$$",
  "combinedFacility": "$$facility$$",
  "tripType": "$$tripType$$",
  "efmFmUserMaster": {
    "eFmFmClientBranchPO": {
      "branchId": "$$facility$$"
    }
  },
  "time": "$$time$$",
  "requestId": "$$requestId$$",
  "employeeId": "67893",
  "areaId": "$$areaId$$",
  "zoneId": 3508,
  "weekOffs": "$$weekOffs$$",
  "updateRegularRequest": "$$updateRegularRequest$$",
  "updateEmployeeRoute":"$$updateEmployeeRoute$$",
  "pickTime":"$$picktime$$",
  "nodalPointId": 1
}