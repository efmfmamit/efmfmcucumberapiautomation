{
  "tripType": "$$tripType$$",
  "userId":"$$userId$$",
  "combinedFacility": "$$facility$$",
  "efmFmUserMaster": {
    "eFmFmClientBranchPO": {
      "branchId": "$$facility$$"
    }
  },
  "requestFrom": "W",
  "startDate": "$$startDate$$",
  "endDate": "$$endDate$$",
  "employeeId": "NO",
  "requestType": "N",
  "locationWaypointsIds": 0,
  "locationFlg": "N",
  "multipleProjectEmpIds": "$$EmpUserId$$",
  "projectId": "$$projectId$$",
  "projectName": "$$projectName$$",
  "multipleEmpIds": 1,
  "reportingManagerUserId": "$$userId$$",
  "pickupTime": 0,
  "pickupShiftId": 0,
  "dropTime": 0,
  "dropShiftId": 0,
  "weekEndFlag": "NO",
  "timeFlg": "S",
  "holidayBookingFlag": "YES",
  "bookingStatusFlag": "YES",
  "holidayDates": "undefined",
  "shiftType": "$$shiftType$$",
  "shiftId": $$shiftId$$,
  "time": "$$shiftTime$$"
}