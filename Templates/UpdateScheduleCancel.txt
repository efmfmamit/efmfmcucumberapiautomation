{
  "branchId": "$$facility$$",
  "employeeId": "$$employeeId$$",
  "updateCancelEmployeeRoster": "$$updateCancelEmployeeRoster$$",
  "startDate": "$$startDate$$",
  "endDate": "$$endDate$$",
  "tripType": "$$tripType$$",
  "time": "00:00",
  "updateEmployeeRoute": "$$updateEmployeeRoute$$",
  "zoneId": 0,
  "nodalPointId": 0,
  "pickTime": "00:04",
  "userId": "$$userId$$",
  "combinedFacility": "$$combinedFacility$$"
}