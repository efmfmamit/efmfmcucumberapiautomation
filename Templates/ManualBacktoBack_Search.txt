{
  "time": "$$time$$",
  "tripType": "$$tripType$$",
  "userId": "$$userId$$",
  "toDate": "$$toDate$$",
  "combinedFacility": "$$facility$$",
  "eFmFmClientBranchPO": {
    "branchId": "$$facility$$"
  },
  "b2bTripType": "$$b2bTripType$$",
  "b2bTime": "$$b2bTime$$",
  "b2bToDate": "$$b2bToDate$$",
  "b2bCombinedFacility": "$$facility$$"
}