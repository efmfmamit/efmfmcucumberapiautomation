{
  "userId": "$$userId$$",
  "combinedFacility": "$$facility$$",
  "efmFmUserMaster": {
    "eFmFmClientBranchPO": {
      "branchId": "$$facility$$"
    }
  },
  "requestId": $$requestid$$,
  "time": "$$time$$",
  "resheduleDate": "$$resheduleDate$$",
   "requestType": "$$shiftType$$",
  "requestStatus": "W"
}